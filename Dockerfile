FROM golang:1.17-alpine as builder
WORKDIR /build
COPY . .
#RUN CGO_ENABLED=1 GOOS=linux go build -a -o /go/bin/booklog -ldflags='-extldflags "-static"' cmd/booklog/main.go

# Arguments Variables (custom)
ARG COMMIT='local'
ARG VERSION='v0.0.0'

RUN apk update && \
    apk --no-cache add ca-certificates


# Build application with custom ldflags
RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 GO111MODULE=on go build \
    -mod vendor \
    -ldflags \
    " \
    -X git.curve.tools/go/app/build.sha=${COMMIT} \
    -X git.curve.tools/go/app/build.version=${VERSION} \
    " -o /app ./booklog.go


# Copy the built application to the docker image
FROM alpine
WORKDIR /root/
COPY --from=builder /app .
ENTRYPOINT ./app
