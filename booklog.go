package main

import (
	"log"

	"gitlab.com/booklog/booklog/internal/config"
	"gitlab.com/booklog/booklog/internal/datastore/driver"
	"gitlab.com/booklog/booklog/internal/datastore/orm"
	"gitlab.com/booklog/booklog/internal/service"
	"gitlab.com/booklog/booklog/internal/web"
)

func main() {
	// Initialize the settings
	settings, err := config.ParseArgs()
	if err != nil {
		log.Fatalf("Configuration error: %v\n", err)
	}

	// Open the connection to the database
	gormDB, err := driver.NewPostgresDriver(settings.DatabaseURL)
	if err != nil {
		log.Fatalf("Error initialising database: %v\n", err)
	}
	db, err := orm.NewDatabase(gormDB, settings.Salt)
	if err != nil {
		log.Fatalf("Error initialising ORM: %v\n", err)
	}

	bl := service.NewBookLogService(db)
	bk := service.NewBookService(db, bl)
	rl := service.NewReadingLogService(db)
	auth := service.NewAuthService(db)

	// Start the booklog service
	log.Println("Starting BookLog Service on port", settings.Port)
	server := web.NewServer(settings, bl, bk, rl, auth)
	log.Fatal(server.Start())
}
