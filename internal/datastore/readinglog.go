package datastore

import "time"

// ReadingLog is the record of when a chapter was read
type ReadingLog struct {
	ID        int64 `gorm:"primaryKey"`
	Username  string
	Book      string
	Chapter   int
	Date      time.Time
	CreatedAt time.Time
	UpdatedAt time.Time
}
