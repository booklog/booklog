package datastore

import (
	"testing"
)

func TestComparePasswords(t *testing.T) {
	type args struct {
		hashedPwd string
		plainPwd  string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{"valid", args{"$2a$04$Vz08aolJ5xfuY6TbXJ25ze7wE.TyQVVfjvnNbEv5SZ4MGTw5.vXSy", "pass123"}, true},
		{"invalid-plain", args{"$2a$04$Vz08aolJ5xfuY6TbXJ25ze7wE.TyQVVfjvnNbEv5SZ4MGTw5.vXSy", "123pass"}, false},
		{"invalid-hash", args{"$2a$04$Vz08aolJ5xfuY6TbXJ25ze7wE.TyQVVfjvnNbEv5SZ4MGTw5.9999", "pass123"}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ComparePasswords(tt.args.hashedPwd, tt.args.plainPwd); got != tt.want {
				t.Errorf("ComparePasswords() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSaltAndHash(t *testing.T) {
	type args struct {
		pwd string
	}
	tests := []struct {
		name    string
		args    args
		check   string
		wantErr bool
	}{
		{"valid", args{"pass123"}, "pass123", false},
		{"invalid", args{"pass123"}, "123pass", true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			hashed, _ := SaltAndHash(tt.args.pwd)

			if got := ComparePasswords(hashed, tt.check); got == tt.wantErr {
				t.Errorf("ComparePasswords() = %v, want %v", got, !tt.wantErr)
			}
		})
	}
}
