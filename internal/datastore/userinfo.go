package datastore

import (
	"time"
)

// UserInfo holds the user details
type UserInfo struct {
	ID        int    `gorm:"primaryKey"`
	Username  string `gorm:"unique"`
	Password  string
	Firstname string
	Lastname  string
	Email     string `gorm:"unique"`
	Role      int
	LastLogin time.Time
	CreatedAt time.Time
	UpdatedAt time.Time
}
