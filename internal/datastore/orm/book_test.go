package orm

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/booklog/booklog/internal/datastore"
)

func TestORM_ListBooks(t *testing.T) {
	t.Run("successfully call helper", func(t *testing.T) {
		orm := &ORM{nil, ""}
		got := orm.ListBooks()
		assert.Len(t, got, 66)
	})
}

func TestORM_GetBook(t *testing.T) {
	t.Run("successfully call helper", func(t *testing.T) {
		orm := &ORM{nil, ""}
		got, err := orm.GetBook("gen")
		assert.NoError(t, err)
		assert.Equal(t, got.Chapters, 50)
	})

	t.Run("successfully call helper with error", func(t *testing.T) {
		orm := &ORM{nil, ""}
		_, err := orm.GetBook("invalid")
		assert.ErrorIs(t, err, datastore.ErrFetchingBook)
	})
}
