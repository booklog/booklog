package orm

import (
	"errors"
	"fmt"
	"log"
	"time"

	"gorm.io/gorm"

	"gitlab.com/booklog/booklog/internal/datastore"
)

const (
	usernameQuery     = "username = ?"
	bookQuery         = "username = ? and book = ?"
	bookQueryYear     = bookQuery + " and date_part('year', date)= ?"
	chapterQuery      = bookQuery + " and chapter = ?"
	usernameYearQuery = "username = ? AND date BETWEEN ? AND ?"
	idQuery           = "username = ? and id = ?"
	limitRecords      = 20
)

// ListReadingLog lists the reading logs for a user and book.
func (orm *ORM) ListReadingLog(user, b string, year int) (logs []datastore.ReadingLog, err error) {
	if year == 0 {
		err = orm.db.Where(bookQuery, user, b).Order("date DESC").Find(&logs).Error
		return logs, checkDatabaseError(err, "error listing reading logs")
	}

	err = orm.db.Where(bookQueryYear, user, b, year).Order("date DESC").Find(&logs).Error
	return logs, checkDatabaseError(err, "error listing reading logs")
}

// ListChapterLog lists the reading logs for a user and book.
func (orm *ORM) ListChapterLog(user, b string, chapter int) (logs []datastore.ReadingLog, err error) {
	err = orm.db.Where(chapterQuery, user, b, chapter).Order("date desc").Limit(limitRecords).Find(&logs).Error
	return logs, checkDatabaseError(err, "error listing chapter logs")
}

// AddReadingLog adds a reading log for a user.
func (orm *ORM) AddReadingLog(user, b string, chapter int) error {
	now := time.Now().UTC()
	record := datastore.ReadingLog{
		Username:  user,
		Book:      b,
		Chapter:   chapter,
		Date:      now,
		CreatedAt: now,
		UpdatedAt: now,
	}

	err := orm.db.Create(&record).Error
	return checkDatabaseError(err, "error creating reading log")
}

// RemoveReadingLog deletes a reading log for a user.
func (orm *ORM) RemoveReadingLog(user, b string, chapter int) error {
	rl := datastore.ReadingLog{}
	err := orm.db.Where(chapterQuery, user, b, chapter).Order("created_at desc").First(&rl).Delete(&rl).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		log.Println("error deleting reading log: %w", err)
		return fmt.Errorf("%w: %s", datastore.ErrRecordNotFound, "error deleting reading log")
	}

	return checkDatabaseError(err, "error deleting reading log")
}

// DeleteReadingLog deletes a reading log for a user.
func (orm *ORM) DeleteReadingLog(user string, id int64) error {
	rl := datastore.ReadingLog{}
	err := orm.db.Where(idQuery, user, id).First(&rl).Delete(&rl).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		log.Println("error deleting reading log: %w", err)
		return fmt.Errorf("%w: %s", datastore.ErrRecordNotFound, "error deleting reading log")
	}

	return checkDatabaseError(err, "error deleting reading log")
}

// LastRead fetches the last reading log for the user.
func (orm *ORM) LastRead(user string) (log datastore.ReadingLog, err error) {
	err = orm.db.Where(usernameQuery, user).Order("date DESC").First(&log).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return datastore.ReadingLog{}, nil
	}

	return log, checkDatabaseError(err, "error fetch last reading log")
}

// CountRead sums the number of the chapters read.
func (orm *ORM) CountRead(user string, year int) (count int64, err error) {
	fromDate := time.Date(year, 1, 1, 000, 0, 0, 0, time.UTC)
	toDate := time.Date(year+1, 1, 1, 000, 0, 0, 0, time.UTC)

	err = orm.db.Model(&datastore.ReadingLog{}).Where(usernameYearQuery, user, fromDate, toDate).Count(&count).Error
	return count, checkDatabaseError(err, "error summing chapters read")
}
