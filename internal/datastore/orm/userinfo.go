package orm

import (
	"log"
	"strings"
	"time"

	"gitlab.com/booklog/booklog/internal/datastore"
	"gitlab.com/booklog/booklog/internal/domain"
)

const (
	userQuery = "lower(email) = ?"
)

// Login verifies a user via the credentials.
func (orm *ORM) Login(email, password string) (domain.UserInfo, error) {
	record := datastore.UserInfo{}
	err := orm.db.Where(userQuery, strings.ToLower(email)).First(&record).Error
	if err != nil {
		return domain.UserInfo{}, checkDatabaseError(err, "error finding user")
	}

	if !datastore.ComparePasswords(record.Password, orm.salt+password) {
		return domain.UserInfo{}, datastore.ErrUserCredentials
	}

	// update the last login on the record
	record.LastLogin = time.Now().UTC()
	err = orm.db.Save(&record).Error

	// return the domain user details, which omits the password details
	return domain.UserInfo{
		Username: record.Username, Email: record.Email, Firstname: record.Firstname, Lastname: record.Lastname, Role: record.Role,
		LastLogin: record.LastLogin, CreatedAt: record.CreatedAt, UpdatedAt: record.UpdatedAt,
	}, checkDatabaseError(err, "error update user last login")
}

// Register registers a new user in the system.
func (orm *ORM) Register(user domain.UserInfo, password string) error {
	pwd, err := datastore.SaltAndHash(orm.salt + password)
	if err != nil {
		log.Println("error encrypting password", err)
		return datastore.ErrPassword
	}

	record := datastore.UserInfo{
		Username:  user.Username,
		Password:  pwd,
		Firstname: user.Firstname,
		Lastname:  user.Lastname,
		Email:     strings.ToLower(user.Email),
		Role:      user.Role,
	}

	err = orm.db.Create(&record).Error
	if uniqueViolationError(err) {
		return datastore.ErrUserExists
	}
	return checkDatabaseError(err, "error creating user")
}
