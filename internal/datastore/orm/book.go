package orm

import (
	"gitlab.com/booklog/booklog/internal/datastore"
)

// ListBooks lists the books in order.
func (orm *ORM) ListBooks() []datastore.Book {
	return datastore.ListBooks()
}

// GetBook gets one book.
func (orm *ORM) GetBook(b string) (datastore.Book, error) {
	return datastore.GetBook(b)
}
