package orm

import (
	"fmt"
	"log"
	"strings"

	"github.com/lib/pq"
	"gorm.io/gorm"

	"gitlab.com/booklog/booklog/internal/datastore"
)

const (
	pgUniqueConstraintCode = pq.ErrorCode("23505")
	sqliteUniqueConstraint = "UNIQUE constraint"
)

// ORM implements a GORM-based datastore.
type ORM struct {
	db   *gorm.DB
	salt string
}

// NewDatabase returns an open database connection.
func NewDatabase(db *gorm.DB, salt string) (*ORM, error) {
	err := db.AutoMigrate(&datastore.UserInfo{}, &datastore.ReadingLog{})
	if err != nil {
		log.Println("error migrating database:", err)
		return nil, err
	}

	return &ORM{db, salt}, nil
}

func checkDatabaseError(err error, code string) error {
	if err != nil {
		log.Printf("%s: %v\n", code, err)
		return fmt.Errorf("%w: %s", datastore.ErrDatabase, code)
	}
	return nil
}

func uniqueViolationError(err error) bool {
	if err == nil {
		return false
	}
	if pqErr, ok := err.(*pq.Error); ok {
		return pqErr.Code == pgUniqueConstraintCode
	}

	if strings.Contains(err.Error(), sqliteUniqueConstraint) {
		return true
	}

	return false
}
