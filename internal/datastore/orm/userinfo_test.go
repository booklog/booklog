package orm

import (
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"

	"gitlab.com/booklog/booklog/internal/datastore"
	"gitlab.com/booklog/booklog/internal/domain"
)

func TestORM_Login(t *testing.T) {
	t.Run("successful and unsuccessful login", func(t *testing.T) {
		t.Cleanup(func() {
			err := os.Remove(sqliteDatabase)
			require.NoError(t, err)
		})

		orm, err := NewDatabase(newDatabase(t), "")
		require.NoError(t, err)

		now := time.Now().UTC()
		err = orm.db.Exec("INSERT INTO user_infos VALUES (1, 'john@example.com', '$2a$04$Vz08aolJ5xfuY6TbXJ25ze7wE.TyQVVfjvnNbEv5SZ4MGTw5.vXSy', 'John','Smith', 'john@example.com', 100, ?,?,?)", now, now, now).Error
		require.NoError(t, err)

		got, err := orm.Login(userJohn, "pass123")
		require.NoError(t, err)
		assert.Equal(t, got.Firstname, "John")
		assert.Equal(t, got.Lastname, "Smith")
		assert.Equal(t, got.Email, "john@example.com")
		assert.Equal(t, got.LastLogin.UTC().Round(time.Second), now.Round(time.Second))

		_, err = orm.Login(userJohn, "badpassword")
		require.ErrorIs(t, err, datastore.ErrUserCredentials)
	})

	t.Run("handle database error", func(t *testing.T) {
		t.Cleanup(func() {
			err := os.Remove(sqliteDatabase)
			require.NoError(t, err)
		})

		// create database without a migration so we generate an error
		db, err := gorm.Open(sqlite.Open(sqliteDatabase), &gorm.Config{})
		require.NoError(t, err)
		orm := &ORM{db, ""}

		_, err = orm.Login(userJohn, "pass123")
		require.ErrorIs(t, err, datastore.ErrDatabase)
	})
}

func TestORM_Register(t *testing.T) {
	const password = "pass123456"
	userNew := domain.UserInfo{
		Username:  "joanna@example.com",
		Email:     "joanna@example.com",
		Firstname: "Joanna",
		Lastname:  "Smith",
		Role:      domain.RoleStandard,
	}

	t.Run("Store user and validate user", func(t *testing.T) {
		t.Cleanup(func() {
			err := os.Remove(sqliteDatabase)
			require.NoError(t, err)
		})

		orm, err := NewDatabase(newDatabase(t), "")
		require.NoError(t, err)

		err = orm.Register(userNew, password)
		require.NoError(t, err)

		// duplicate
		err = orm.Register(userNew, password)
		require.ErrorIs(t, err, datastore.ErrUserExists)

		// validate user
		_, err = orm.Login(userNew.Username, password)
		require.NoError(t, err)
	})
}
