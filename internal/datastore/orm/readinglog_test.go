package orm

import (
	"os"
	"testing"

	"github.com/stretchr/testify/require"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"

	"gitlab.com/booklog/booklog/internal/datastore"
)

const (
	sqliteDatabase = "test.db"
	userJohn       = "john@example.com"
)

func newDatabase(t *testing.T) *gorm.DB {
	db, err := gorm.Open(sqlite.Open(sqliteDatabase), &gorm.Config{SkipDefaultTransaction: true})
	require.NoError(t, err)
	return db
}

func TestORM_ReadingLogFlow(t *testing.T) {
	t.Cleanup(func() {
		err := os.Remove(sqliteDatabase)
		require.NoError(t, err)
	})

	t.Run("check reading log flow", func(t *testing.T) {
		orm, err := NewDatabase(newDatabase(t), "")
		require.NoError(t, err)

		err = orm.AddReadingLog(userJohn, "jos", 1)
		require.NoError(t, err)

		err = orm.AddReadingLog(userJohn, "jos", 1)
		require.NoError(t, err)

		// check the last read
		lastLog1, err := orm.LastRead(userJohn)
		require.NoError(t, err)
		require.Equal(t, lastLog1.Book, "jos")
		require.Equal(t, lastLog1.Chapter, 1)

		err = orm.AddReadingLog(userJohn, "jer", 1)
		require.NoError(t, err)

		// check the last read
		lastLog2, err := orm.LastRead(userJohn)
		require.NoError(t, err)
		require.Equal(t, lastLog2.Book, "jer")
		require.Equal(t, lastLog2.Chapter, 1)

		// check logs for Joshua
		logsJoshua, err := orm.ListReadingLog(userJohn, "jos", 0)
		require.NoError(t, err)
		require.Len(t, logsJoshua, 2)

		// check logs for Jeremiah
		logsJeremiah, err := orm.ListReadingLog(userJohn, "jer", 0)
		require.NoError(t, err)
		require.Len(t, logsJeremiah, 1)

		// check logs for Genesis
		logsGenesis, err := orm.ListReadingLog(userJohn, "gen", 0)
		require.NoError(t, err)
		require.Len(t, logsGenesis, 0)

		// check logs for Joshua 1
		logsJoshua1, err := orm.ListChapterLog(userJohn, "jos", 1)
		require.NoError(t, err)
		require.Len(t, logsJoshua1, 2)

		// check logs for Jeremiah 1
		logsJeremiah1, err := orm.ListChapterLog(userJohn, "jer", 1)
		require.NoError(t, err)
		require.Len(t, logsJeremiah1, 1)

		// delete by ID
		err = orm.DeleteReadingLog(userJohn, logsJeremiah1[0].ID)
		require.NoError(t, err)

		// check logs for Jeremiah 1
		logsJeremiah1, err = orm.ListChapterLog(userJohn, "jer", 1)
		require.NoError(t, err)
		require.Len(t, logsJeremiah1, 0)

		// check logs for Genesis 1
		logsGenesis1, err := orm.ListChapterLog(userJohn, "gen", 1)
		require.NoError(t, err)
		require.Len(t, logsGenesis1, 0)

		// Remove a log for Joshua 1
		err = orm.RemoveReadingLog(userJohn, "jos", 1)
		require.NoError(t, err)

		// check logs for Joshua 1
		logsJoshua1, err = orm.ListChapterLog(userJohn, "jos", 1)
		require.NoError(t, err)
		require.Len(t, logsJoshua1, 1)

		// Remove a log for unread chapter
		err = orm.RemoveReadingLog(userJohn, "gen", 1)
		require.ErrorIs(t, err, datastore.ErrRecordNotFound)

		// invalid delete by ID
		err = orm.DeleteReadingLog("jack", 99)
		require.ErrorIs(t, err, datastore.ErrRecordNotFound)

		// invalid last read
		last, err := orm.LastRead("jack")
		require.NoError(t, err)
		require.Equal(t, datastore.ReadingLog{}, last)
	})
}

func TestORM_DatabaseError(t *testing.T) {
	t.Cleanup(func() {
		err := os.Remove(sqliteDatabase)
		require.NoError(t, err)
	})

	t.Run("check database error handling", func(t *testing.T) {
		// create database without a migration so we generate an error
		db, err := gorm.Open(sqlite.Open(sqliteDatabase), &gorm.Config{})
		require.NoError(t, err)
		orm := &ORM{db, ""}

		err = orm.AddReadingLog(userJohn, "jos", 1)
		require.ErrorIs(t, err, datastore.ErrDatabase)

		_, err = orm.LastRead(userJohn)
		require.ErrorIs(t, err, datastore.ErrDatabase)

		_, err = orm.CountRead(userJohn, 2022)
		require.ErrorIs(t, err, datastore.ErrDatabase)
	})
}
