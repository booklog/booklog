package datastore

import (
	"errors"
)

// datastore errors
var (
	ErrDatabase        = errors.New("error accessing database")
	ErrFetchingBook    = errors.New("cannot find book with code")
	ErrRecordNotFound  = errors.New("record not found")
	ErrUserCredentials = errors.New("the user or password are invalid")
	ErrUserExists      = errors.New("the user already exists")
	ErrPassword        = errors.New("error encrypting password")
)
