package datastore

import (
	"testing"
)

func TestBooks(t *testing.T) {
	tests := []struct {
		name  string
		book  string
		index int
		count int
	}{
		{"genesis", "gen", 0, 66},
		{"matthew", "mat", 39, 66},
	}

	for _, tt := range tests {
		bks := ListBooks()
		if len(books) != tt.count {
			t.Errorf("%s: expected %d records, got: %d\n", tt.name, tt.count, len(books))
		}
		b := bks[tt.index]
		if b.Code != tt.book {
			t.Errorf("%s: expected %s, got: %s\n", tt.name, tt.book, b.Code)
		}
	}
}

func TestGetBook(t *testing.T) {
	tests := []struct {
		name     string
		book     string
		chapters int
	}{
		{"genesis", "gen", 50},
		{"matthew", "mat", 28},
		{"invalid", "invalid", 0},
	}

	for _, tt := range tests {
		book, err := GetBook(tt.book)
		if err == nil && tt.book == "invalid" {
			t.Errorf("%s: expected failure, got success", tt.name)
		}
		if err != nil && tt.book != "invalid" {
			t.Errorf("%s: expected %s, got: %s\n", tt.name, tt.book, book.Code)
		}
		if book.Chapters != tt.chapters {
			t.Errorf("%s: expected %d chapters, got: %d\n", tt.name, tt.chapters, book.Chapters)
		}
	}
}
