package datastore

import (
	_ "github.com/mattn/go-sqlite3" // sqlite driver

	"gitlab.com/booklog/booklog/internal/domain"
)

// Datastore interface for the database logic.
type Datastore interface {
	ReadingLogStore
	BookStore
	AuthStore
}

// ReadingLogStore defines the methods of the reading log store.
type ReadingLogStore interface {
	ListReadingLog(user, b string, year int) ([]ReadingLog, error)
	ListChapterLog(user, b string, chapter int) ([]ReadingLog, error)
	LastRead(user string) (ReadingLog, error)
	CountRead(user string, year int) (count int64, err error)

	AddReadingLog(user, b string, chapter int) error
	RemoveReadingLog(user, b string, chapter int) error
	DeleteReadingLog(user string, id int64) error
}

// BookStore defines the methods of the reading book store.
type BookStore interface {
	ListBooks() []Book
	GetBook(b string) (Book, error)
}

// AuthStore defines the methods of the user authorisation store.
type AuthStore interface {
	Login(email, password string) (domain.UserInfo, error)
	Register(user domain.UserInfo, password string) error
}
