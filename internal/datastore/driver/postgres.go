package driver

import (
	"database/sql"
	"fmt"
	"time"

	_ "github.com/lib/pq" // SQL driver
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

// NewPostgresDriver creates a postgres database connection using GORM.
func NewPostgresDriver(url string) (*gorm.DB, error) {
	sqlDB, err := sql.Open("postgres", url)
	if err != nil {
		return nil, fmt.Errorf("error opening database: %w", err)
	}
	sqlDB.SetConnMaxIdleTime(time.Minute * 5)
	sqlDB.SetConnMaxLifetime(time.Hour)

	return gorm.Open(postgres.New(
		postgres.Config{Conn: sqlDB}),
		&gorm.Config{
			SkipDefaultTransaction: true,
			Logger:                 logger.Default.LogMode(logger.Silent),
		})
}
