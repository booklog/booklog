package datastore

import (
	"log"

	"golang.org/x/crypto/bcrypt"
)

// SaltAndHash encrypts the password for storage
func SaltAndHash(pwd string) (string, error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(pwd), bcrypt.MinCost)
	if err != nil {
		return "", err
	}
	return string(hash), nil
}

// ComparePasswords verifies an entered password
func ComparePasswords(hashedPwd, plainPwd string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hashedPwd), []byte(plainPwd))
	if err != nil {
		log.Println("Error comparing passwords:", err)
		return false
	}

	return true
}
