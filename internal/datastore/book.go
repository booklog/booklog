package datastore

import (
	"fmt"
)

// Book holds the details of a book
type Book struct {
	Code     string
	Name     string
	Chapters int
}

var books = []Book{
	{Code: "gen", Name: "Genesis", Chapters: 50},
	{Code: "ex", Name: "Exodus", Chapters: 40},
	{Code: "lev", Name: "Leviticus", Chapters: 27},
	{Code: "num", Name: "Numbers", Chapters: 36},
	{Code: "deut", Name: "Deuteronomy", Chapters: 34},
	{Code: "jos", Name: "Joshua", Chapters: 24},
	{Code: "ruth", Name: "Ruth", Chapters: 4},
	{Code: "jud", Name: "Judges", Chapters: 21},
	{Code: "1sam", Name: "1 Samuel", Chapters: 31},
	{Code: "2sam", Name: "2 Samuel", Chapters: 24},
	{Code: "1kin", Name: "1 Kings", Chapters: 22},
	{Code: "2kin", Name: "2 Kings", Chapters: 25},
	{Code: "1chr", Name: "1 Chronicles", Chapters: 29},
	{Code: "2chr", Name: "2 Chronicles", Chapters: 36},
	{Code: "neh", Name: "Nehemiah", Chapters: 13},
	{Code: "ezra", Name: "Ezra", Chapters: 10},
	{Code: "est", Name: "Esther", Chapters: 10},
	{Code: "job", Name: "Job", Chapters: 42},
	{Code: "psa", Name: "Psalms", Chapters: 150},
	{Code: "pro", Name: "Proverbs", Chapters: 31},
	{Code: "ecc", Name: "Ecclesiastes", Chapters: 12},
	{Code: "ss", Name: "Song of Solomon", Chapters: 8},
	{Code: "isa", Name: "Isaiah", Chapters: 66},
	{Code: "jer", Name: "Jeremiah", Chapters: 52},
	{Code: "lam", Name: "Lamentations", Chapters: 5},
	{Code: "eze", Name: "Ezekiel", Chapters: 48},
	{Code: "dan", Name: "Daniel", Chapters: 12},
	{Code: "hos", Name: "Hosea", Chapters: 14},
	{Code: "joel", Name: "Joel", Chapters: 3},
	{Code: "amos", Name: "Amos", Chapters: 9},
	{Code: "oba", Name: "Obadiah", Chapters: 1},
	{Code: "jon", Name: "Jonah", Chapters: 4},
	{Code: "mic", Name: "Micah", Chapters: 7},
	{Code: "nah", Name: "Nahum", Chapters: 3},
	{Code: "hab", Name: "Habakkuk", Chapters: 3},
	{Code: "zep", Name: "Zephaniah", Chapters: 3},
	{Code: "hag", Name: "Haggai", Chapters: 2},
	{Code: "zec", Name: "Zechariah", Chapters: 14},
	{Code: "mal", Name: "Malachi", Chapters: 4},
	{Code: "mat", Name: "Matthew", Chapters: 28},
	{Code: "mark", Name: "Mark", Chapters: 16},
	{Code: "luke", Name: "Luke", Chapters: 24},
	{Code: "john", Name: "John", Chapters: 21},
	{Code: "acts", Name: "Acts", Chapters: 28},
	{Code: "rom", Name: "Romans", Chapters: 16},
	{Code: "1cor", Name: "1 Corinthians", Chapters: 16},
	{Code: "2cor", Name: "2 Corinthians", Chapters: 13},
	{Code: "gal", Name: "Galatians", Chapters: 6},
	{Code: "eph", Name: "Ephesians", Chapters: 6},
	{Code: "phi", Name: "Philippians", Chapters: 4},
	{Code: "col", Name: "Colossians", Chapters: 4},
	{Code: "1th", Name: "1 Thessalonians", Chapters: 5},
	{Code: "2th", Name: "2 Thessalonians", Chapters: 3},
	{Code: "1tim", Name: "1 Timothy", Chapters: 6},
	{Code: "2tim", Name: "2 Timothy", Chapters: 4},
	{Code: "tit", Name: "Titus", Chapters: 3},
	{Code: "philem", Name: "Philemon", Chapters: 1},
	{Code: "heb", Name: "Hebrews", Chapters: 13},
	{Code: "jam", Name: "James", Chapters: 5},
	{Code: "1pet", Name: "1 Peter", Chapters: 5},
	{Code: "2pet", Name: "2 Peter", Chapters: 3},
	{Code: "1joh", Name: "1 John", Chapters: 5},
	{Code: "2joh", Name: "2 John", Chapters: 1},
	{Code: "3joh", Name: "3 John", Chapters: 1},
	{Code: "jude", Name: "Jude", Chapters: 1},
	{Code: "rev", Name: "Revelation", Chapters: 22},
}

// ListBooks lists the books in order
func ListBooks() []Book {
	return books
}

// GetBook gets one book
func GetBook(b string) (Book, error) {
	for _, book := range books {
		if b == book.Code {
			return book, nil
		}
	}
	return Book{}, fmt.Errorf("%w: %s", ErrFetchingBook, b)
}
