package config

import (
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestParseArgs(t *testing.T) {
	t.Run("default-settings", func(t *testing.T) {
		os.Setenv(EnvPort, "8080")
		os.Setenv(EnvSecretKey, "secretkey")
		os.Setenv(EnvDatabaseURL, "sqlite3://biblelog.db")
		os.Setenv(EnvPasswordSalt, "passwordSecret")

		got, err := ParseArgs()
		require.NoError(t, err)
		require.Equal(t, Settings{
			DocRoot:       DefaultDocRoot,
			IndexTemplate: DefaultIndexTemplate,
			Port:          "8080",
			Secret:        "secretkey",
			Salt:          "passwordSecret",
			DatabaseURL:   "sqlite3://biblelog.db",
			TokenExpiry:   time.Hour * 2,
		}, *got)
	})
}

func TestParseArgs1(t *testing.T) {
	tests := []struct {
		name  string
		unset string
	}{
		{"missing port", EnvPort},
		{"missing secret key", EnvSecretKey},
		{"missing password salt", EnvPasswordSalt},
		{"missing database URL", EnvDatabaseURL},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			os.Setenv(EnvPort, "8080")
			os.Setenv(EnvSecretKey, "secretkey")
			os.Setenv(EnvDatabaseURL, "sqlite3://biblelog.db")
			os.Setenv(EnvPasswordSalt, "passwordSecret")

			err := os.Unsetenv(tt.unset)
			require.NoError(t, err)

			_, err = ParseArgs()
			require.Error(t, err)
			assert.Contains(t, err.Error(), tt.unset)
		})
	}
}
