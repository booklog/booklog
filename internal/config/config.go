package config

import (
	"fmt"
	"log"
	"os"
	"time"
)

// Default settings
const (
	DefaultDocRoot       = "./static"
	DefaultIndexTemplate = "index.html"

	EnvPort         = "PORT"
	EnvSecretKey    = "SECRET"
	EnvCSRFKey      = "CSRF"
	EnvPasswordSalt = "PASSWORD_SECRET"
	EnvDatabaseURL  = "DATABASE_URL"
	EnvTokenExpiry  = "TOKEN_EXPIRY"
)

// Settings defines the application configuration
type Settings struct {
	DocRoot       string
	IndexTemplate string
	Port          string
	Secret        string
	CSRF          string
	Salt          string
	DatabaseURL   string
	TokenExpiry   time.Duration
}

// ParseArgs checks the command line arguments
func ParseArgs() (*Settings, error) {
	// Get the port env var
	port := os.Getenv(EnvPort)
	if len(port) == 0 {
		return nil, fmt.Errorf("the '%s' environment variable must be set", EnvPort)
	}

	// Get the database env var
	dbURL := os.Getenv(EnvDatabaseURL)
	if len(dbURL) == 0 {
		return nil, fmt.Errorf("the '%s' environment variable must be set", EnvDatabaseURL)
	}

	// Get the secret env var
	secret := os.Getenv(EnvSecretKey)
	if len(secret) == 0 {
		return nil, fmt.Errorf("the '%s' environment variable must be set", EnvSecretKey)
	}

	// Get the secret env var
	csrf := os.Getenv(EnvCSRFKey)
	if len(csrf) == 0 {
		fmt.Printf("WARNING: the '%s' environment variable is not set\n", EnvCSRFKey)
	}

	// Get the salt env var
	salt := os.Getenv(EnvPasswordSalt)
	if len(salt) == 0 {
		return nil, fmt.Errorf("The '%s' environment variable must be set", EnvPasswordSalt)
	}

	// token expiry configuration
	tokenExpiry, err := time.ParseDuration(os.Getenv(EnvTokenExpiry))
	if err != nil {
		log.Println("Invalid token expiry. Defaulting to 2h")
		tokenExpiry = time.Hour * 2
	}

	return &Settings{
		Port:          port,
		Secret:        secret,
		CSRF:          csrf,
		Salt:          salt,
		DatabaseURL:   dbURL,
		DocRoot:       DefaultDocRoot,
		IndexTemplate: DefaultIndexTemplate,
		TokenExpiry:   tokenExpiry,
	}, nil
}
