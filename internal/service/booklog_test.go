package service

import (
	"os"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/booklog/booklog/internal/domain"
)

func TestBookLog(t *testing.T) {
	tests := []struct {
		name     string
		method   string
		user     string
		book     string
		success  bool
		chapters int
		logCount int
	}{
		{"genesis", "GET", "john@example.com", "gen", true, 3, 4},
		{"1cor", "GET", "jane@example.com", "1cor", true, 1, 1},
		{"revelation", "GET", "john@example.com", "rev", true, 0, 0},
		{"invalid", "GET", "john@example.com", "invalid", false, 0, 0},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			db := setupDatabase(t)
			srv := NewBookLogService(db)
			t.Cleanup(func() {
				err := os.Remove(sqliteDatabase)
				require.NoError(t, err)
			})

			bl, err := srv.Get(tt.user, tt.book, 0)

			if err != nil && tt.success {
				t.Errorf("%s: expected success, got: %v\n", tt.name, err)
			}
			if err == nil && !tt.success {
				t.Errorf("%s: expected failure, got success\n", tt.name)
			}

			// Check the book
			if bl.Code != tt.book && tt.success {
				t.Errorf("%s: expected %s, got: %s\n", tt.name, tt.book, bl.Code)
			}

			chapters := 0
			for i := 0; i < len(bl.Read); i++ {
				if bl.Read[i] > 0 {
					chapters = chapters + 1
				}
			}
			if chapters != tt.chapters && tt.success {
				t.Errorf("%s: expected %d read, got: %d\n", tt.name, tt.chapters, chapters)
			}

			// Get the logs
			logs, err := srv.Logs(tt.user, tt.book, 0)
			if err != nil && tt.success {
				t.Errorf("%s: expected success, got: %v\n", tt.name, err)
			}
			if len(logs) != tt.logCount {
				t.Errorf("%s: expected %d read, got: %d\n", tt.name, tt.logCount, len(logs))
			}
		})
	}
}

func Test_bookLogAdapter_LastRead(t *testing.T) {
	t.Run("last read log", func(t *testing.T) {
		db := setupDatabase(t)
		srv := NewBookLogService(db)
		t.Cleanup(func() {
			err := os.Remove(sqliteDatabase)
			require.NoError(t, err)
		})

		// no logs
		got, err := srv.LastRead("does-not-exist@example.com")
		require.NoError(t, err)
		require.Equal(t, domain.ReadingLog{}, got)
	})

}

func Test_bookLogAdapter_ChaptersRead(t *testing.T) {
	t.Run("chapters read", func(t *testing.T) {
		db := setupDatabase(t)
		srv := NewBookLogService(db)
		t.Cleanup(func() {
			err := os.Remove(sqliteDatabase)
			require.NoError(t, err)
		})

		// no logs
		got, err := srv.ChaptersRead("does-not-exist@example.com", 2022)
		require.NoError(t, err)
		require.Equal(t, int64(0), got)

		// some logs
		got1, err := srv.ChaptersRead("john@example.com", 2019)
		require.NoError(t, err)
		require.Equal(t, int64(4), got1)
	})
}
