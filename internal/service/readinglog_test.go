package service

import (
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestReadingLogAddRemoveList(t *testing.T) {
	tests := []struct {
		name    string
		book    string
		chapter int
		records int
	}{
		{"jos-add-remove-list", "jos", 5, 1},
		{"gen-add-remove-list", "gen", 14, 3},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			db := setupDatabase(t)
			t.Cleanup(func() {
				err := os.Remove(sqliteDatabase)
				require.NoError(t, err)
			})

			// Add
			rl := NewReadingLogService(db)
			err := rl.AddReadingLog("john@example.com", tt.book, tt.chapter)
			if err != nil {
				t.Errorf("%s: %v\n", tt.name, err)
				return
			}

			// List
			records, err := rl.ListReadingLog("john@example.com", tt.book, tt.chapter)
			if err != nil {
				t.Errorf("%s: %v\n", tt.name, err)
				return
			}
			if len(records) != tt.records {
				t.Errorf("Expected %d records, got: %d\n", tt.records, len(records))
				return
			}

			// Remove
			err = rl.RemoveReadingLog("john@example.com", tt.book, tt.chapter)
			if err != nil {
				t.Errorf("%s: %v\n", tt.name, err)
				return
			}

			// Add
			err = rl.AddReadingLog("john@example.com", tt.book, tt.chapter)
			if err != nil {
				t.Errorf("%s: %v\n", tt.name, err)
				return
			}

			// List
			records1, err := rl.ListReadingLog("john@example.com", tt.book, tt.chapter)
			if err != nil {
				t.Errorf("%s: %v\n", tt.name, err)
				return
			}

			// Remove by ID
			err = rl.DeleteReadingLog("john@example.com", records1[0].ID)
			if err != nil {
				t.Errorf("%s: %v\n", tt.name, err)
				return
			}
		})
	}
}
