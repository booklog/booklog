package service

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"

	"gitlab.com/booklog/booklog/internal/datastore"
	"gitlab.com/booklog/booklog/internal/datastore/orm"
)

func setupDatabase(t *testing.T) datastore.Datastore {
	t.Helper()
	gormDB := newDatabase(t)
	db, err := orm.NewDatabase(gormDB, "")
	require.NoError(t, err)

	now := time.Now().UTC()
	err = gormDB.Exec("INSERT INTO user_infos VALUES (1, 'john@example.com', '$2a$04$Vz08aolJ5xfuY6TbXJ25ze7wE.TyQVVfjvnNbEv5SZ4MGTw5.vXSy', 'John','Smith', 'john@example.com', 100, ?,?,?)", now, now, now).Error
	require.NoError(t, err)
	err = gormDB.Exec("INSERT INTO user_infos VALUES (2, 'jane@example.com', '$2a$04$LDqfXm6/lgug2C9ChDNxaeIzQ.qE1zzr7nbzsucO7KVz.E6oTE2S6', 'Jane','Smith', 'jane@example.com', 100, ?,?,?)", now, now, now).Error
	require.NoError(t, err)

	const logs = `
INSERT INTO reading_logs VALUES (1, 'john@example.com', 'gen', 14, '2019-02-01T10:10:12Z', '2019-02-01T10:10:12Z', '2019-02-01T10:10:12Z');
INSERT INTO reading_logs VALUES (2, 'jane@example.com', '1cor', 13,'2019-12-01T13:10:12Z', '2019-02-01T10:10:12Z', '2019-02-01T10:10:12Z');
INSERT INTO reading_logs VALUES (3, 'john@example.com', 'gen', 14, '2019-02-02T13:22:12Z', '2019-02-01T10:10:12Z', '2019-02-01T10:10:12Z');
INSERT INTO reading_logs VALUES (4, 'john@example.com', 'gen', 16, '2019-02-03T17:10:12Z', '2019-02-01T10:10:12Z', '2019-02-01T10:10:12Z');
INSERT INTO reading_logs VALUES (5, 'john@example.com', 'gen', 15, '2019-02-02T19:17:12Z', '2019-02-01T10:10:12Z', '2019-02-01T10:10:12Z');
`
	err = gormDB.Exec(logs).Error
	require.NoError(t, err)
	return db
}
