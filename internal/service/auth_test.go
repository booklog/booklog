package service

import (
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"

	"gitlab.com/booklog/booklog/internal/datastore"
	"gitlab.com/booklog/booklog/internal/domain"
)

const (
	sqliteDatabase = "test.db"
)

func newDatabase(t *testing.T) *gorm.DB {
	db, err := gorm.Open(sqlite.Open(sqliteDatabase), &gorm.Config{SkipDefaultTransaction: true})
	require.NoError(t, err)
	return db
}

func Test_authAdapter_Login(t *testing.T) {
	tests := []struct {
		name     string
		username string
		password string
		wantErr  bool
	}{
		{"valid", "john@example.com", "pass123", false},
		{"invalid-password", "john@example.com", "invalid", true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			db := setupDatabase(t)
			t.Cleanup(func() {
				err := os.Remove(sqliteDatabase)
				require.NoError(t, err)
			})

			aa := NewAuthService(db)
			got, err := aa.Login(tt.username, tt.password)
			if tt.wantErr {
				require.Error(t, err)
			} else {
				require.NoError(t, err)
				require.Equal(t, tt.username, got.Username)
			}
		})
	}
}

func Test_authAdapter_Register(t *testing.T) {
	type args struct {
		email     string
		password  string
		firstname string
		lastname  string
	}

	userNew := args{"joanna@example.com", "pass56789", "Joanna", "Smith"}
	expectNew := domain.UserInfo{
		Username:  "", // cannot compare this value as it gets generated
		Email:     userNew.email,
		Firstname: userNew.firstname,
		Lastname:  userNew.lastname,
		Role:      domain.RoleStandard,
	}
	userDuplicate := args{"john@example.com", "pass56789", "John", "Doe"}
	expectDuplicate := domain.UserInfo{
		Username:  "", // cannot compare this value as it gets generated
		Email:     userDuplicate.email,
		Firstname: userDuplicate.firstname,
		Lastname:  userDuplicate.lastname,
		Role:      domain.RoleStandard,
	}
	userInvalid := args{"joanna@example.com", "pass56", "Joanna", "Smith"}

	tests := []struct {
		name    string
		args    args
		want    domain.UserInfo
		wantErr error
	}{
		{"success registering a new user", userNew, expectNew, nil},
		{"fail with a duplicate user", userDuplicate, expectDuplicate, datastore.ErrUserExists},
		{"fail with an invalid user", userInvalid, domain.UserInfo{}, ErrUserValidation},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			db := setupDatabase(t)
			t.Cleanup(func() {
				err := os.Remove(sqliteDatabase)
				require.NoError(t, err)
			})

			aa := NewAuthService(db)
			got, err := aa.Register(tt.args.email, tt.args.password, tt.args.firstname, tt.args.lastname)
			require.ErrorIs(t, err, tt.wantErr)
			if tt.wantErr != nil {
				return
			}

			assert.NotEmpty(t, got.Username)
			got.Username = "" // clear the username so we can do the equality check
			assert.Equal(t, tt.want, got)
		})
	}
}

func Test_validateUserFields(t *testing.T) {
	tests := []struct {
		testname  string
		email     string
		password  string
		firstname string
		lastname  string
		wantErr   error
		message   string
	}{
		{"valid", "john@example.com", "pass1234568", "John", "Smith", nil, ""},
		{"no email", "", "pass1234568", "John", "Smith", ErrUserValidation, "email not entered"},
		{"no firstname", "john@example.com", "pass1234568", "", "Smith", ErrUserValidation, "firstname not entered"},
		{"no lastname", "john@example.com", "pass1234568", "John", "", ErrUserValidation, "lastname not entered"},
		{"no password", "john@example.com", "", "John", "Smith", ErrUserValidation, "password not entered"},
		{"bad password", "john@example.com", "pass", "John", "Smith", ErrUserValidation, "password must be"},
		{"short email", "jo", "pass1234568", "John", "Smith", ErrUserValidation, "email is not a valid email address"},
		{"bad email", "john@", "pass1234568", "John", "Smith", ErrUserValidation, "email is not a valid email address"},
		{"long email", fmt.Sprintf("john%0254d@example.com", 0), "pass1234568", "John", "Smith", ErrUserValidation, "email is not a valid email address"},
	}
	for _, tt := range tests {
		t.Run(tt.testname, func(t *testing.T) {
			err := validateUserFields(tt.email, tt.password, tt.firstname, tt.lastname)
			require.ErrorIs(t, err, tt.wantErr)
			assert.Contains(t, fmt.Sprintf("%v", err), tt.message)
		})
	}
}
