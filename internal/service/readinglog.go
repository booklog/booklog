package service

import (
	"gitlab.com/booklog/booklog/internal/datastore"
)

// ReadingLogService provides APIs for the reading log
type ReadingLogService interface {
	AddReadingLog(user, b string, chapter int) error
	RemoveReadingLog(user, b string, chapter int) error
	DeleteReadingLog(user string, id int64) error
	ListReadingLog(user, b string, chapter int) ([]datastore.ReadingLog, error)
}

type readingLogAdapter struct {
	DB datastore.Datastore
}

// NewReadingLogService creates a new reading log service
func NewReadingLogService(db datastore.Datastore) ReadingLogService {
	return &readingLogAdapter{DB: db}
}

// AddReadingLog adds a readinglog entry
func (rl *readingLogAdapter) AddReadingLog(user, b string, chapter int) error {
	// TODO: validate the book and the chapter
	return rl.DB.AddReadingLog(user, b, chapter)
}

// RemoveReadingLog deletes a reading log entry
func (rl *readingLogAdapter) RemoveReadingLog(user, b string, chapter int) error {
	// TODO: validate the book and the chapter
	return rl.DB.RemoveReadingLog(user, b, chapter)
}

// DeleteReadingLog deletes a reading log entry by ID.
func (rl *readingLogAdapter) DeleteReadingLog(user string, id int64) error {
	return rl.DB.DeleteReadingLog(user, id)
}

// ListReadingLog lists the reading log for a chapter
func (rl *readingLogAdapter) ListReadingLog(user, b string, chapter int) ([]datastore.ReadingLog, error) {
	// TODO: validate the book and the chapter
	return rl.DB.ListChapterLog(user, b, chapter)
}
