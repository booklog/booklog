package service

import (
	"gitlab.com/booklog/booklog/internal/datastore"
	"gitlab.com/booklog/booklog/internal/domain"
)

// BookLogService provides APIs for a book's reading chart
type BookLogService interface {
	Get(user, book string, year int) (domain.Book, error)
	Logs(user, book string, year int) ([]domain.ReadingLog, error)
	ReadsForBook(user string, bk datastore.Book, year int) (domain.Book, error)

	LastRead(user string) (domain.ReadingLog, error)
	ChaptersRead(user string, year int) (int64, error)
}

type bookLogAdapter struct {
	DB datastore.Datastore
}

// NewBookLogService creates a new book log service
func NewBookLogService(db datastore.Datastore) BookLogService {
	return &bookLogAdapter{DB: db}
}

// Get gets the formatted reading log for a book
func (bl *bookLogAdapter) Get(user, book string, year int) (domain.Book, error) {
	// Get the book
	bk, err := bl.DB.GetBook(book)
	if err != nil {
		return domain.Book{}, err
	}

	return bl.ReadsForBook(user, bk, year)
}

// Logs fetches the for a user and book
func (bl *bookLogAdapter) Logs(user, book string, year int) ([]domain.ReadingLog, error) {
	logs, err := bl.DB.ListReadingLog(user, book, year)
	if err != nil {
		return nil, err
	}

	records := make([]domain.ReadingLog, 0)
	for _, l := range logs {
		rec := domainReadingLog(l)
		records = append(records, rec)
	}
	return records, nil
}

func domainReadingLog(l datastore.ReadingLog) domain.ReadingLog {
	return domain.ReadingLog{
		ID:      l.ID,
		User:    l.Username,
		Book:    l.Book,
		Chapter: l.Chapter,
		Date:    l.Date,
	}
}

func (bl *bookLogAdapter) ReadsForBook(user string, bk datastore.Book, year int) (domain.Book, error) {
	b := domain.Book{
		Code:     bk.Code,
		Name:     bk.Name,
		Chapters: bk.Chapters,
		Read:     make([]int, bk.Chapters),
	}

	// Get the reading logs for the book
	logs, err := bl.DB.ListReadingLog(user, bk.Code, year)
	if err != nil {
		return domain.Book{}, err
	}

	// Populate the book log with the reads-per-chapter
	for _, l := range logs {
		b.Read[l.Chapter-1] = b.Read[l.Chapter-1] + 1
	}
	return b, nil
}

func (bl *bookLogAdapter) LastRead(user string) (domain.ReadingLog, error) {
	log, err := bl.DB.LastRead(user)
	if err != nil {
		return domain.ReadingLog{}, err
	}
	return domainReadingLog(log), nil
}

func (bl *bookLogAdapter) ChaptersRead(user string, year int) (int64, error) {
	return bl.DB.CountRead(user, year)
}
