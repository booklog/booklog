package service

import (
	"errors"
	"fmt"
	"regexp"

	"github.com/google/uuid"

	"gitlab.com/booklog/booklog/internal/datastore"
	"gitlab.com/booklog/booklog/internal/domain"
)

// ErrUserValidation error for the auth service.
var (
	ErrUserValidation = errors.New("error validating user details")
)

const (
	// MinPasswordLength is the minimum password length that is accepted.
	MinPasswordLength = 8
)

var emailRegex = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")

// AuthService provides APIs for authn and authz.
type AuthService interface {
	Login(email, password string) (domain.UserInfo, error)
	Register(email, password, firstname, lastname string) (domain.UserInfo, error)
}

type authAdapter struct {
	DB datastore.Datastore
}

// NewAuthService creates an auth service.
func NewAuthService(db datastore.Datastore) AuthService {
	return &authAdapter{DB: db}
}

// Login verifies a user by their credentials.
func (aa *authAdapter) Login(email, password string) (domain.UserInfo, error) {
	return aa.DB.Login(email, password)
}

// Register a new user in the system.
func (aa *authAdapter) Register(email, password, firstname, lastname string) (domain.UserInfo, error) {
	if err := validateUserFields(email, password, firstname, lastname); err != nil {
		return domain.UserInfo{}, err
	}

	// create the user
	user := domain.UserInfo{
		Username:  uuid.NewString(),
		Email:     email,
		Firstname: firstname,
		Lastname:  lastname,
		Role:      domain.RoleStandard,
	}
	err := aa.DB.Register(user, password)

	return user, err
}

func validateUserFields(email string, password string, firstname, lastname string) error {
	if firstname == "" {
		return fmt.Errorf("%w: firstname not entered", ErrUserValidation)
	}
	if lastname == "" {
		return fmt.Errorf("%w: lastname not entered", ErrUserValidation)
	}
	if email == "" {
		return fmt.Errorf("%w: email not entered", ErrUserValidation)
	}
	if password == "" {
		return fmt.Errorf("%w: password not entered", ErrUserValidation)
	}
	if len(password) < MinPasswordLength {
		return fmt.Errorf("%w: password must be %d characters or longer", ErrUserValidation, MinPasswordLength)
	}
	if !validateEmail(email) {
		return fmt.Errorf("%w: email is not a valid email address", ErrUserValidation)
	}
	return nil
}

func validateEmail(e string) bool {
	if len(e) < 3 || len(e) > 254 {
		return false
	}
	return emailRegex.MatchString(e)
}
