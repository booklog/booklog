package service

import (
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

func Test_bookAdapter_List_ReadSummary(t *testing.T) {
	tests := []struct {
		name string
		user string

		chapter    int
		readAmount int
		wantSize   int
		wantErr    bool
	}{
		{"list-read1", "john@example.com", 14, 2, 66, false},
		{"list-read2", "jane@example.com", 14, 0, 66, false},
		{"list-invalid", "invalid", 14, 0, 66, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			db := setupDatabase(t)
			t.Cleanup(func() {
				err := os.Remove(sqliteDatabase)
				require.NoError(t, err)
			})

			bl := NewBookService(db, &bookLogAdapter{db})
			got, err := bl.List()
			if (err != nil) != tt.wantErr {
				t.Errorf("bookAdapter.List() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if len(got) != tt.wantSize {
				t.Errorf("bookAdapter.List() = %d, want %d", len(got), tt.wantSize)
			}

			got1, err := bl.ReadSummary(tt.user, 0)
			if (err != nil) != tt.wantErr {
				t.Errorf("bookAdapter.ReadSummary() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if len(got1) != tt.wantSize {
				t.Errorf("bookAdapter.ReadSummary() = %d, want %d", len(got1), tt.wantSize)
			}
			if len(got1[0].Read) != 50 {
				t.Errorf("bookAdapter.ReadSummary() read = %d, want %d", len(got1[0].Read), 50)
			}
			if got1[0].Read[tt.chapter-1] != tt.readAmount {
				t.Errorf("bookAdapter.ReadSummary() read chapter = %d, want %d", got1[0].Read[tt.chapter-1], tt.readAmount)
			}
		})
	}
}
