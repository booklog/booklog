package service

import (
	"gitlab.com/booklog/booklog/internal/datastore"
	"gitlab.com/booklog/booklog/internal/domain"
)

// BookService provides APIs for the reading log
type BookService interface {
	List() ([]domain.Book, error)
	ReadSummary(user string, year int) ([]domain.Book, error)
}

type bookAdapter struct {
	DB      datastore.Datastore
	BookLog BookLogService
}

// NewBookService creates a new book service
func NewBookService(db datastore.Datastore, bookLog BookLogService) BookService {
	return &bookAdapter{DB: db, BookLog: bookLog}
}

// List fetches the list of books
func (bl *bookAdapter) List() ([]domain.Book, error) {
	bb := make([]domain.Book, 66)
	books := bl.DB.ListBooks()

	for i, b := range books {
		bk := domain.Book{
			Code:     b.Code,
			Name:     b.Name,
			Chapters: b.Chapters,
		}
		bb[i] = bk
	}
	return bb, nil
}

// ReadSummary fetches the summary of each book and the chapters read.
func (bl *bookAdapter) ReadSummary(user string, year int) ([]domain.Book, error) {
	bb := make([]domain.Book, 66)
	books := bl.DB.ListBooks()

	for i, b := range books {
		bk, err := bl.BookLog.ReadsForBook(user, b, year)
		if err != nil {
			return nil, err
		}

		bb[i] = bk
	}
	return bb, nil
}
