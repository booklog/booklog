package web

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"

	"gitlab.com/booklog/booklog/internal/domain"
)

// UserClaims holds the JWT custom claims for a user
const (
	ClaimsUsername         = "username"
	ClaimsEmail            = "email"
	ClaimsFirstname        = "firstname"
	ClaimsLastname         = "lastname"
	ClaimsRole             = "role"
	StandardClaimExpiresAt = "exp"
)

// NewJWTToken creates a new JWT from the login response
func NewJWTToken(secret string, userinfo *domain.UserInfo, expires int64) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)

	claims := token.Claims.(jwt.MapClaims)
	claims[ClaimsUsername] = userinfo.Username
	claims[ClaimsFirstname] = userinfo.Firstname
	claims[ClaimsLastname] = userinfo.Lastname
	claims[ClaimsEmail] = userinfo.Email
	claims[ClaimsRole] = userinfo.Role
	claims[StandardClaimExpiresAt] = expires

	if len(secret) == 0 {
		return "", errors.New("JWT secret empty value. Please configure it properly")
	}

	tokenString, err := token.SignedString([]byte(secret))
	if err != nil {
		log.Printf("Error signing the JWT: %v", err.Error())
	}
	return tokenString, err
}

// JWTExtractor extracts the JWT from a request and returns the token string.
// The token is not verified.
func JWTExtractor(r *http.Request) (string, error) {
	// Get the JWT from the header
	jwtToken := r.Header.Get("Authorization")
	splitToken := strings.Split(jwtToken, " ")
	if len(splitToken) != 2 {
		jwtToken = ""
	} else {
		jwtToken = splitToken[1]
	}

	// Check the cookie if we don't have a bearer token in the header
	if jwtToken == "" {
		cookie, err := r.Cookie(JWTCookie)
		if err != nil {
			return "", fmt.Errorf("cannot find the JWT")
		}
		jwtToken = cookie.Value
	}

	return jwtToken, nil
}
