package web

import (
	"encoding/json"
	"io"
	"net/http"
	"time"

	"github.com/gorilla/csrf"
)

// JWTCookie is the name of the cookie used to store the JWT
const JWTCookie = "X-Auth-Token"

// LoginRequest is the request to authenticate a user
type LoginRequest struct {
	Email    string `json:"email,omitempty"`
	Password string `json:"password,omitempty"`
}

// Token verifies that the user is logged in. The middleware will handle an unsuccessful response.
func (srv Controller) Token(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("X-CSRF-Token", csrf.Token(r))
	formatStandardResponse("", "", w)
}

// Login is the API handler to authenticate a user
func (srv Controller) Login(w http.ResponseWriter, r *http.Request) {
	req := srv.decodeLogin(w, r)
	if req == nil {
		return
	}

	user, err := srv.AuthService.Login(req.Email, req.Password)
	if err != nil {
		formatStandardResponse("login", err.Error(), w)
		return
	}

	// Create the JWT from the user details and store it in the reply
	token, err := NewJWTToken(srv.Settings.Secret, &user, time.Now().Add(srv.Settings.TokenExpiry).Unix())
	if err != nil {
		formatStandardResponse("token", err.Error(), w)
		return
	}
	addJWTCookie(token, w)

	formatRecordResponse(user, w)
}

func (srv Controller) decodeLogin(w http.ResponseWriter, r *http.Request) *LoginRequest {
	// Decode the JSON body
	req := LoginRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	switch {
	// Check we have some data
	case err == io.EOF:
		formatStandardResponse("data", "No data supplied.", w)
		return nil
		// Check for parsing errors
	case err != nil:
		formatStandardResponse("decode-json", err.Error(), w)
		return nil
	}
	return &req
}

// addJWTCookie sets the JWT as a cookie
func addJWTCookie(jwtToken string, w http.ResponseWriter) {
	// Set the JWT as a bearer token
	// (In practice, the cookie will be used more as clicking on a page link will not send the auth header)
	w.Header().Set("Authorization", "Bearer "+jwtToken)

	expireCookie := time.Now().Add(time.Hour * 1)
	cookie := http.Cookie{Name: JWTCookie, Value: jwtToken, Expires: expireCookie, HttpOnly: true}
	http.SetCookie(w, &cookie)
}

// Logout is the API handler to logout a user.
func (srv Controller) Logout(w http.ResponseWriter, r *http.Request) {
	w.Header().Del("Authorization")

	// expire the cookie (it cannot be deleted)
	expireCookie := time.Unix(0, 0)
	cookie := http.Cookie{Name: JWTCookie, Value: "", Expires: expireCookie, HttpOnly: true}
	http.SetCookie(w, &cookie)

	http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
}
