package web

import (
	"bytes"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestController_Register(t *testing.T) {
	// Set up the mock services
	settings := getSettings()
	bk := &mockBook{}
	rl := &mockReadingLog{}
	auth := &mockAuth{}

	validUser := []byte(`{"email":"john", "password":"pass123", "name":"John Smith"}`)
	invalidUser := []byte(`<>`)
	incompleteUser := []byte(`{"email":"john", "name":"John Smith"}`)

	tests := []struct {
		name       string
		data       []byte
		wantStatus int
	}{
		{"valid", validUser, http.StatusOK},
		{"invalid", invalidUser, http.StatusBadRequest},
		{"incomplete", incompleteUser, http.StatusBadRequest},
		{"invalid-empty", nil, http.StatusBadRequest},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			srv := NewServer(settings, &mockBookLog{}, bk, rl, auth)
			w := sendRequest("POST", "/v1/register", bytes.NewReader(tt.data), srv)
			assert.Equal(t, tt.wantStatus, w.Code)
		})
	}
}
