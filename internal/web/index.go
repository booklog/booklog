package web

import (
	"html/template"
	"log"
	"net/http"
	"path/filepath"
)

// Page is the page details for the web application
type Page struct {
	Title string
}

// Index is the front page of the web application
func (srv Controller) Index(w http.ResponseWriter, r *http.Request) {
	p := filepath.Join(srv.Settings.DocRoot, srv.Settings.IndexTemplate)

	t, err := template.ParseFiles(p)
	if err != nil {
		log.Printf("Error loading the application template: %v\n", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = t.Execute(w, nil)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
