package web

import (
	"bytes"
	"fmt"
	"net/http"
	"testing"

	"gitlab.com/booklog/booklog/internal/domain"
)

type mockAuth struct{}

// Login mocks the user login
func (m *mockAuth) Login(email, password string) (domain.UserInfo, error) {
	if email != "john@example.com" || password != "pass123" {
		return domain.UserInfo{}, fmt.Errorf("MOCK login error")
	}

	return domain.UserInfo{
		Username: "john", Email: "john@example.com", Firstname: "John", Lastname: "Smith", Role: 100,
	}, nil
}

func (m *mockAuth) Register(user, password, first, last string) (domain.UserInfo, error) {
	if password == "" {
		return domain.UserInfo{}, fmt.Errorf("MOCK login error")
	}
	return domain.UserInfo{}, nil
}

func TestController_Login(t *testing.T) {
	// Set up the mock services
	settings := getSettings()
	bk := &mockBook{}
	rl := &mockReadingLog{}
	auth := &mockAuth{}

	validUser := []byte(`{"email":"john@example.com", "password":"pass123"}`)
	invalidUser := []byte(`{"email":"john@example.com", "password":"invalid"}`)

	tests := []struct {
		name       string
		data       []byte
		wantStatus int
	}{
		{"valid", validUser, http.StatusOK},
		{"invalid", invalidUser, http.StatusBadRequest},
		{"invalid-data", []byte(`\u1000`), http.StatusBadRequest},
		{"invalid-empty", nil, http.StatusBadRequest},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			srv := NewServer(settings, &mockBookLog{}, bk, rl, auth)
			w := sendRequest("POST", "/v1/login", bytes.NewReader(tt.data), srv)
			if w.Code != tt.wantStatus {
				t.Errorf("Login() expected HTTP status '%d', got: %v", tt.wantStatus, w.Code)
			}
		})
	}
}
