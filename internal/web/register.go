package web

import (
	"encoding/json"
	"io"
	"net/http"
	"strings"
)

// RegisterRequest is the request to register a new user.
type RegisterRequest struct {
	Email     string `json:"email,omitempty"`
	Password  string `json:"password,omitempty"`
	Firstname string `json:"firstname,omitempty"`
	Lastname  string `json:"lastname,omitempty"`
}

// Register a new user with the system.
func (srv Controller) Register(w http.ResponseWriter, r *http.Request) {
	req := srv.decodeUser(w, r)
	if req == nil {
		return
	}

	email := strings.TrimSpace(req.Email)
	password := strings.TrimSpace(req.Password)
	firstname := strings.TrimSpace(req.Firstname)
	lastname := strings.TrimSpace(req.Lastname)

	user, err := srv.AuthService.Register(email, password, firstname, lastname)
	if err != nil {
		formatStandardResponse("register", err.Error(), w)
		return
	}

	formatRecordResponse(user, w)
}

func (srv Controller) decodeUser(w http.ResponseWriter, r *http.Request) *RegisterRequest {
	// Decode the JSON body
	req := RegisterRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	switch {
	// Check we have some data
	case err == io.EOF:
		formatStandardResponse("data", "No data supplied.", w)
		return nil
		// Check for parsing errors
	case err != nil:
		formatStandardResponse("decode-json", err.Error(), w)
		return nil
	}
	return &req
}
