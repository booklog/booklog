package web

import (
	"bytes"
	"fmt"
	"net/http"
	"testing"
	"time"

	"gitlab.com/booklog/booklog/internal/datastore"
	"gitlab.com/booklog/booklog/internal/domain"
)

type mockBook struct {
	wantErr bool
}

func (bk *mockBook) List() ([]domain.Book, error) {
	if bk.wantErr {
		return nil, fmt.Errorf("MOCK error list")
	}
	return []domain.Book{
		{Code: "gen", Name: "Genesis", Chapters: 50},
		{Code: "ruth", Name: "Ruth", Chapters: 4},
	}, nil
}

func (bk *mockBook) ReadSummary(user string, year int) ([]domain.Book, error) {
	if bk.wantErr {
		return nil, fmt.Errorf("MOCK error list")
	}
	return []domain.Book{
		{Code: "gen", Name: "Genesis", Chapters: 50},
		{Code: "ruth", Name: "Ruth", Chapters: 4, Read: []int{1, 0, 2, 4}},
	}, nil
}

type mockBookLog struct{}

func (bl *mockBookLog) Get(user, book string, year int) (domain.Book, error) {
	if book == "ruth" {
		return domain.Book{Code: "ruth", Name: "Ruth", Read: []int{1, 0, 5, 4}}, nil
	}
	return domain.Book{}, fmt.Errorf("MOCK book log error")
}

func (bl *mockBookLog) Logs(user, book string, year int) ([]domain.ReadingLog, error) {
	if book == "ruth" {
		return []domain.ReadingLog{{User: "ruth", Book: "Ruth", Chapter: 1, Date: time.Now()}}, nil
	}
	return nil, fmt.Errorf("MOCK book log error")
}

func (bl *mockBookLog) ReadsForBook(user string, bk datastore.Book, year int) (domain.Book, error) {
	return domain.Book{
		Code:     bk.Code,
		Name:     bk.Name,
		Chapters: bk.Chapters,
		Read:     make([]int, bk.Chapters),
	}, nil
}

func (bl *mockBookLog) LastRead(user string) (domain.ReadingLog, error) {
	return domain.ReadingLog{}, nil
}

func (bl *mockBookLog) ChaptersRead(user string, year int) (int64, error) {
	return 125, nil
}

type mockReadingLog struct{}

func (rl *mockReadingLog) AddReadingLog(user, b string, chapter int) error {
	if b == "ruth" {
		return nil
	}
	return fmt.Errorf("MOCK reading log error")
}

func (rl *mockReadingLog) RemoveReadingLog(user, b string, chapter int) error {
	if b == "ruth" {
		return nil
	}
	return fmt.Errorf("MOCK reading log error")
}

func (rl *mockReadingLog) DeleteReadingLog(user string, id int64) error {
	if id == 1 {
		return nil
	}
	return fmt.Errorf("MOCK reading log error")
}

func (rl *mockReadingLog) ListReadingLog(user, b string, chapter int) ([]datastore.ReadingLog, error) {
	if b == "ruth" {
		return []datastore.ReadingLog{
			{ID: 1, Date: time.Now()},
		}, nil
	}
	return nil, fmt.Errorf("MOCK reading log error")
}

func TestController_BookList(t *testing.T) {
	// Set up the mock services
	settings := getSettings()
	bl := &mockBookLog{}
	rl := &mockReadingLog{}
	auth := &mockAuth{}

	tests := []struct {
		name        string
		servicesErr bool
		wantStatus  int
	}{
		{"valid", false, http.StatusOK},
		{"invalid", true, http.StatusBadRequest},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			srv := NewServer(settings, bl, &mockBook{tt.servicesErr}, rl, auth)
			w := sendRequestWithJWT("POST", "/v1/books", nil, srv)
			if w.Code != tt.wantStatus {
				t.Errorf("BookList() expected HTTP status '%d', got: %v", tt.wantStatus, w.Code)
			}
		})
	}
}

func TestController_ReadSummary(t *testing.T) {
	// Set up the mock services
	settings := getSettings()
	bl := &mockBookLog{}
	rl := &mockReadingLog{}
	auth := &mockAuth{}

	validReq := []byte(`{"year":2021}`)
	emptyReq := []byte(``)
	invalidReq := []byte(`\u1000`)

	tests := []struct {
		name        string
		servicesErr bool
		data        []byte
		wantStatus  int
	}{
		{"valid", false, validReq, http.StatusOK},
		{"invalid", true, validReq, http.StatusBadRequest},
		{"empty", false, emptyReq, http.StatusOK},
		{"invalid-data", false, invalidReq, http.StatusBadRequest},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			srv := NewServer(settings, bl, &mockBook{tt.servicesErr}, rl, auth)
			w := sendRequestWithJWT("POST", "/v1/books/summary", bytes.NewReader(tt.data), srv)
			if w.Code != tt.wantStatus {
				t.Errorf("BookList() expected HTTP status '%d', got: %v", tt.wantStatus, w.Code)
			}
		})
	}
}
