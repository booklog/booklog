package web

import (
	"log"
	"net/http"
	"time"
)

// Logger to pre-process web service requests
func Logger(inner http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		log.Printf(
			"%s\t%s\t%s",
			r.Method,
			r.RequestURI,
			time.Since(start),
		)

		inner.ServeHTTP(w, r)
	})
}
