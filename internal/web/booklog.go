package web

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"

	"gitlab.com/booklog/booklog/internal/domain"
)

// BookLogRequest is the request for getting a reading log for a book.
type BookLogRequest struct {
	Code string `json:"code,omitempty"`
	Year int    `json:"year,omitempty"`
}

// BookLogUpdateRequest the request for adding/updating/deleting a reading log.
type BookLogUpdateRequest struct {
	Action  string    `json:"action,omitempty"`
	Code    string    `json:"code,omitempty"`
	Chapter int       `json:"chapter,omitempty"`
	When    time.Time `json:"when,omitempty"`
}

// ChapterLogRequest the request for getting a reading log for a chapter.
type ChapterLogRequest struct {
	Code    string `json:"code,omitempty"`
	Chapter int    `json:"chapter,omitempty"`
}

// BookLog is the API handler to get the reading log for a book.
func (srv Controller) BookLog(w http.ResponseWriter, r *http.Request) {
	// get the user details from the context
	user, ok := r.Context().Value(ctxUserKey).(domain.UserInfo)
	if !ok {
		formatStandardResponse("user", ErrUserUnauthorised.Error(), w)
		return
	}

	req := srv.decodeBooklog(w, r)
	if req == nil {
		return
	}

	log, err := srv.BookLogService.Get(user.Username, req.Code, req.Year)
	if err != nil {
		formatStandardResponse("list", err.Error(), w)
		return
	}

	formatRecordResponse(log, w)
}

// BookLogUpdate is the API handler to update the reading log for a book.
func (srv Controller) BookLogUpdate(w http.ResponseWriter, r *http.Request) {
	// get the user details from the context
	user, ok := r.Context().Value(ctxUserKey).(domain.UserInfo)
	if !ok {
		formatStandardResponse("user", ErrUserUnauthorised.Error(), w)
		return
	}

	req := srv.decodeBooklogUpdate(w, r)
	if req == nil {
		return
	}

	var err error
	switch req.Action {
	case "add":
		err = srv.ReadingLogService.AddReadingLog(user.Username, req.Code, req.Chapter)
	case "remove":
		err = srv.ReadingLogService.RemoveReadingLog(user.Username, req.Code, req.Chapter)
	default:
		err = fmt.Errorf("invalid action for book log")
	}
	if err != nil {
		formatStandardResponse("update", err.Error(), w)
		return
	}

	formatStandardResponse("", "", w)
}

// BookLogRemove is the API handler to remove the reading log for a book.
func (srv Controller) BookLogRemove(w http.ResponseWriter, r *http.Request) {
	// get the user details from the context
	user, ok := r.Context().Value(ctxUserKey).(domain.UserInfo)
	if !ok {
		formatStandardResponse("user", ErrUserUnauthorised.Error(), w)
		return
	}

	vars := mux.Vars(r)
	id, err := strconv.ParseInt(vars["id"], 10, 64)
	if err != nil {
		formatStandardResponse("param", "invalid booklog ID", w)
		return
	}

	err = srv.ReadingLogService.DeleteReadingLog(user.Username, id)
	if err != nil {
		formatStandardResponse("remove", err.Error(), w)
		return
	}

	formatStandardResponse("", "", w)
}

// ChapterLog returns the reading log for a chapter
func (srv Controller) ChapterLog(w http.ResponseWriter, r *http.Request) {
	// get the user details from the context
	user, ok := r.Context().Value(ctxUserKey).(domain.UserInfo)
	if !ok {
		formatStandardResponse("user", ErrUserUnauthorised.Error(), w)
		return
	}

	req := srv.decodeChapterLog(w, r)
	if req == nil {
		return
	}

	records, err := srv.ReadingLogService.ListReadingLog(user.Username, req.Code, req.Chapter)
	if err != nil {
		formatStandardResponse("log", err.Error(), w)
		return
	}

	formatRecordsResponse(records, w)
}

// BookLogs returns the reading logs for a user and book
func (srv Controller) BookLogs(w http.ResponseWriter, r *http.Request) {
	// get the user details from the context
	user, ok := r.Context().Value(ctxUserKey).(domain.UserInfo)
	if !ok {
		formatStandardResponse("user", ErrUserUnauthorised.Error(), w)
		return
	}

	req := srv.decodeBooklog(w, r)
	if req == nil {
		return
	}

	records, err := srv.BookLogService.Logs(user.Username, req.Code, req.Year)
	if err != nil {
		formatStandardResponse("logs", err.Error(), w)
		return
	}
	formatRecordsResponse(records, w)
}

func (srv Controller) decodeBooklog(w http.ResponseWriter, r *http.Request) *BookLogRequest {
	// Decode the JSON body
	req := BookLogRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	switch {
	// Check we have some data
	case err == io.EOF:
		formatStandardResponse("data", "No data supplied.", w)
		return nil
		// Check for parsing errors
	case err != nil:
		formatStandardResponse("decode-json", err.Error(), w)
		return nil
	}
	return &req
}

func (srv Controller) decodeBooklogUpdate(w http.ResponseWriter, r *http.Request) *BookLogUpdateRequest {
	// Decode the JSON body
	req := BookLogUpdateRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	switch {
	// Check we have some data
	case err == io.EOF:
		formatStandardResponse("data", "No data supplied.", w)
		return nil
		// Check for parsing errors
	case err != nil:
		formatStandardResponse("decode-json", err.Error(), w)
		return nil
	}
	return &req
}

func (srv Controller) decodeChapterLog(w http.ResponseWriter, r *http.Request) *ChapterLogRequest {
	// Decode the JSON body
	req := ChapterLogRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	switch {
	// Check we have some data
	case err == io.EOF:
		formatStandardResponse("data", "No data supplied.", w)
		return nil
		// Check for parsing errors
	case err != nil:
		formatStandardResponse("decode-json", err.Error(), w)
		return nil
	}
	return &req
}
