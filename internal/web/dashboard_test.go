package web

import (
	"bytes"
	"net/http"
	"testing"
)

func TestController_Dashboard(t *testing.T) {
	// Set up the mock services
	settings := getSettings()
	bl := &mockBookLog{}
	rl := &mockReadingLog{}
	auth := &mockAuth{}

	validReq := []byte(`{"year":2021}`)
	badReq := []byte(`{"year":invalid}`)
	emptyReq := []byte(``)

	tests := []struct {
		name       string
		data       []byte
		wantStatus int
	}{
		{"valid", validReq, http.StatusOK},
		{"empty", emptyReq, http.StatusOK},
		{"bad", badReq, http.StatusBadRequest},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			srv := NewServer(settings, bl, &mockBook{}, rl, auth)
			w := sendRequestWithJWT("POST", "/v1/dashboard", bytes.NewReader(tt.data), srv)
			if w.Code != tt.wantStatus {
				t.Errorf("Dashboard expected HTTP status '%d', got: %v", tt.wantStatus, w.Code)
			}
		})
	}
}
