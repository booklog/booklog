package web

import (
	"encoding/json"
	"io"
	"net/http"

	"gitlab.com/booklog/booklog/internal/domain"
)

// BookInfo details of a book
type BookInfo struct {
	Code     string `json:"code,omitempty"`
	Name     string `json:"name,omitempty"`
	Chapters int    `json:"chapters,omitempty"`
}

// SummaryRequest is the request to get the overview records.
type SummaryRequest struct {
	Year int `json:"year,omitempty"`
}

// BookList is the API handler to return books
func (srv Controller) BookList(w http.ResponseWriter, r *http.Request) {
	// Get the books
	books, err := srv.BookService.List()
	if err != nil {
		formatStandardResponse("list", err.Error(), w)
		return
	}

	// Format the response
	formatRecordsResponse(books, w)
}

// ReadSummary is the API handler to return the reading summary
func (srv Controller) ReadSummary(w http.ResponseWriter, r *http.Request) {
	// get the user details from the context
	user, ok := r.Context().Value(ctxUserKey).(domain.UserInfo)
	if !ok {
		formatStandardResponse("user", ErrUserUnauthorised.Error(), w)
		return
	}

	req := decodeSummary(w, r)
	if req == nil {
		return
	}

	logs, err := srv.BookService.ReadSummary(user.Username, req.Year)
	if err != nil {
		formatStandardResponse("list", err.Error(), w)
		return
	}

	// Format the response
	formatRecordsResponse(logs, w)
}

func decodeSummary(w http.ResponseWriter, r *http.Request) *SummaryRequest {
	// Decode the JSON body
	req := SummaryRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	switch {
	case err == io.EOF:
		// allow empty
		return &req
	case err != nil:
		formatStandardResponse("decode-json", err.Error(), w)
		return nil
	}
	return &req
}
