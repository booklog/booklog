package web

import (
	"net/http"

	"gitlab.com/booklog/booklog/internal/domain"
)

// Dashboard is the API handler to get data for the dashboard.
func (srv Controller) Dashboard(w http.ResponseWriter, r *http.Request) {
	// get the user details from the context
	user, ok := r.Context().Value(ctxUserKey).(domain.UserInfo)
	if !ok {
		formatStandardResponse("user", ErrUserUnauthorised.Error(), w)
		return
	}

	req := decodeSummary(w, r)
	if req == nil {
		return
	}

	dash := srv.fetchData(user, req)
	formatRecordResponse(dash, w)
}

// fetchData calls the service layer concurrently for the different dashboard stats.
func (srv Controller) fetchData(user domain.UserInfo, req *SummaryRequest) domain.Dashboard {
	chLastRead := make(chan domain.ReadingLog)
	chChapters := make(chan int64)
	chSummary := make(chan []domain.Book)

	go func() {
		lastRead, _ := srv.BookLogService.LastRead(user.Username)
		chLastRead <- lastRead
	}()

	go func() {
		chaptersRead, _ := srv.BookLogService.ChaptersRead(user.Username, req.Year)
		chChapters <- chaptersRead
	}()

	go func() {
		summary, _ := srv.BookService.ReadSummary(user.Username, req.Year)
		chSummary <- summary
	}()

	var lastRead domain.ReadingLog
	var chaptersRead int64
	var summary []domain.Book

	for i := 0; i < 3; i++ {
		select {
		case lastRead = <-chLastRead:
			break
		case chaptersRead = <-chChapters:
			break
		case summary = <-chSummary:
			break
		}
	}

	dash := domain.Dashboard{
		LastRead:     lastRead,
		ChaptersRead: chaptersRead,
		Summary:      summary,
	}
	return dash
}
