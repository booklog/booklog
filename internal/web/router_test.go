package web

import (
	"io"
	"net/http"
	"net/http/httptest"
	"time"

	"gitlab.com/booklog/booklog/internal/config"
	"gitlab.com/booklog/booklog/internal/domain"
)

func sendRequest(method, url string, data io.Reader, srv *Controller) *httptest.ResponseRecorder {
	w := httptest.NewRecorder()
	r, _ := http.NewRequest(method, url, data)

	srv.Router().ServeHTTP(w, r)
	return w
}

func sendRequestWithJWT(method, url string, data io.Reader, srv *Controller) *httptest.ResponseRecorder {
	w := httptest.NewRecorder()
	r, _ := http.NewRequest(method, url, data)

	user := domain.UserInfo{
		Username:  "john@example.com",
		Email:     "john@example.com",
		Firstname: "John",
		Lastname:  "Smith",
		Role:      100,
	}
	token, _ := NewJWTToken(srv.Settings.Secret, &user, time.Now().Add(time.Hour*24).Unix())
	r.Header.Set("Authorization", "Bearer "+token)

	srv.Router().ServeHTTP(w, r)
	return w
}

func getSettings() *config.Settings {
	return &config.Settings{
		Secret: "JWTSecret",
	}
}
