package web

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"

	"gitlab.com/booklog/booklog/internal/config"
	"gitlab.com/booklog/booklog/internal/service"
)

// Controller is the API web service.
type Controller struct {
	Settings          *config.Settings
	BookLogService    service.BookLogService
	BookService       service.BookService
	ReadingLogService service.ReadingLogService
	AuthService       service.AuthService
	Auth              Auth
}

// NewServer returns a new web controller.
func NewServer(settings *config.Settings, bl service.BookLogService, bk service.BookService, rl service.ReadingLogService, auth service.AuthService) *Controller {
	ctrl := Controller{
		Settings:          settings,
		BookLogService:    bl,
		BookService:       bk,
		ReadingLogService: rl,
		AuthService:       auth,
		Auth:              NewAuthMiddleware([]byte(settings.Secret), []byte(settings.CSRF)),
	}

	return &ctrl
}

// Start the web service.
func (srv Controller) Start() error {
	listenOn := fmt.Sprintf("0.0.0.0:%s", srv.Settings.Port)
	fmt.Printf("Starting service on port %s\n", listenOn)
	return http.ListenAndServe(listenOn, srv.Router())
}

// Router returns the application router.
func (srv Controller) Router() *mux.Router {
	// Start the web service router
	router := mux.NewRouter()

	router.Handle("/v1/token", srv.Auth.CSRF(Logger(http.HandlerFunc(srv.Token)))).Methods("GET")
	router.Handle("/v1/login", Logger(http.HandlerFunc(srv.Login))).Methods("POST")
	router.Handle("/v1/register", Logger(http.HandlerFunc(srv.Register))).Methods("POST")
	router.Handle("/logout", Logger(http.HandlerFunc(srv.Logout))).Methods("GET")

	router.Handle("/v1/books", srv.Auth.CSRF(srv.Auth.Authorise(Logger(http.HandlerFunc(srv.BookList))))).Methods("POST")
	router.Handle("/v1/books/summary", srv.Auth.CSRF(srv.Auth.Authorise(Logger(http.HandlerFunc(srv.ReadSummary))))).Methods("POST")
	router.Handle("/v1/dashboard", srv.Auth.CSRF(srv.Auth.Authorise(Logger(http.HandlerFunc(srv.Dashboard))))).Methods("POST")
	router.Handle("/v1/booklogs", srv.Auth.CSRF(srv.Auth.Authorise(Logger(http.HandlerFunc(srv.BookLogs))))).Methods("POST")
	router.Handle("/v1/booklog", srv.Auth.CSRF(srv.Auth.Authorise(Logger(http.HandlerFunc(srv.BookLog))))).Methods("POST")
	router.Handle("/v1/booklog", srv.Auth.CSRF(srv.Auth.Authorise(Logger(http.HandlerFunc(srv.BookLogUpdate))))).Methods("PUT")
	router.Handle("/v1/booklog/{id}", srv.Auth.CSRF(srv.Auth.Authorise(Logger(http.HandlerFunc(srv.BookLogRemove))))).Methods("DELETE")
	router.Handle("/v1/chapterlog", srv.Auth.CSRF(srv.Auth.Authorise(Logger(http.HandlerFunc(srv.ChapterLog))))).Methods("POST")

	// Serve the static path
	fs := http.StripPrefix("/static/", http.FileServer(http.Dir(srv.Settings.DocRoot)))
	router.PathPrefix("/static/").Handler(fs)

	// Default path is the index page
	router.Handle("/", Logger(http.HandlerFunc(srv.Index))).Methods("GET")
	router.Handle("/update", Logger(http.HandlerFunc(srv.Index))).Methods("GET")
	router.Handle("/logs", Logger(http.HandlerFunc(srv.Index))).Methods("GET")
	router.NotFoundHandler = Logger(http.HandlerFunc(srv.Index))

	return router
}
