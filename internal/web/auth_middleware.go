package web

import (
	"context"
	"errors"
	"fmt"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/csrf"

	"gitlab.com/booklog/booklog/internal/domain"
)

// Auth is the implementation of the authorisation middleware.
type Auth struct {
	secret []byte
	csrf   []byte
}

// UserKey is the context key type for the user details.
type UserKey string

const (
	ctxUserKey UserKey = "user"
)

// ErrUserUnauthorised indicates that the user auth is missing or invalid.
var ErrUserUnauthorised = errors.New("user is not authorised")

// NewAuthMiddleware constructs the authorisation middleware.
func NewAuthMiddleware(secret, csrf []byte) Auth {
	return Auth{secret: secret, csrf: csrf}
}

// Authorise middleware checks that the requester is authorised to proceed.
func (a Auth) Authorise(inner http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// decode the user from the JWT
		user, err := a.GetUserFromJWT(w, r)
		if err != nil {
			formatStandardResponse("user", err.Error(), w)
			return
		}

		// add user details to the context
		ctx := context.WithValue(r.Context(), ctxUserKey, user)
		inner.ServeHTTP(w, r.WithContext(ctx))
	})
}

// CSRF is the middleware to protect a URI against cross-site request forgery.
func (a Auth) CSRF(inner http.Handler) http.Handler {
	if len(a.csrf) == 0 {
		return inner
	}
	c := csrf.Protect(a.csrf, csrf.Secure(true)) //csrf.Secure(true))
	return c(inner)
}

// GetUserFromJWT retrieves the user details from the JSON Web Token
func (a Auth) GetUserFromJWT(w http.ResponseWriter, r *http.Request) (domain.UserInfo, error) {
	tokenString, err := JWTExtractor(r)
	if err != nil {
		return domain.UserInfo{}, err
	}

	token, err := a.VerifyJWT(tokenString)
	if err != nil {
		return domain.UserInfo{}, err
	}
	if token == nil {
		return domain.UserInfo{}, fmt.Errorf("error finding the authentication token")
	}

	claims := token.Claims.(jwt.MapClaims)
	username := claims[ClaimsUsername].(string)
	email := claims[ClaimsEmail].(string)
	role := int(claims[ClaimsRole].(float64))

	// Set up the bearer token in the header
	w.Header().Set("Authorization", "Bearer "+tokenString)

	return domain.UserInfo{
		Username: username,
		Email:    email,
		Role:     role,
	}, nil
}

// VerifyJWT checks that we have a valid token
func (a Auth) VerifyJWT(jwtToken string) (*jwt.Token, error) {
	token, err := jwt.Parse(jwtToken, func(token *jwt.Token) (interface{}, error) {
		return a.secret, nil
	})
	return token, err
}
