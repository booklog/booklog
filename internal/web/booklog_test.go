package web

import (
	"bytes"
	"fmt"
	"net/http"
	"testing"
)

func TestController_BookLog(t *testing.T) {
	// Set up the mock services
	settings := getSettings()
	bk := &mockBook{}
	rl := &mockReadingLog{}
	auth := &mockAuth{}

	validBook := []byte(`{"code":"ruth"}`)
	invalidBook := []byte(`{"code":"invalid"}`)

	tests := []struct {
		name       string
		data       []byte
		wantStatus int
	}{
		{"valid", validBook, http.StatusOK},
		{"invalid", invalidBook, http.StatusBadRequest},
		{"invalid-data", []byte(`\u1000`), http.StatusBadRequest},
		{"invalid-empty", nil, http.StatusBadRequest},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			srv := NewServer(settings, &mockBookLog{}, bk, rl, auth)
			w := sendRequestWithJWT("POST", "/v1/booklog", bytes.NewReader(tt.data), srv)
			if w.Code != tt.wantStatus {
				t.Errorf("BookLog() expected HTTP status '%d', got: %v", tt.wantStatus, w.Code)
			}
		})
	}
}

func TestController_BookLogUpdate(t *testing.T) {
	// Set up the mock services
	settings := getSettings()
	bk := &mockBook{}
	rl := &mockReadingLog{}
	auth := &mockAuth{}

	validBook := []byte(`{"action":"add", "code":"ruth"}`)
	invalidBook := []byte(`{"action":"add","code":"invalid"}`)
	removeBook := []byte(`{"action":"remove","code":"ruth"}`)
	invalidAction := []byte(`{"action":"invalid","code":"ruth"}`)

	tests := []struct {
		name       string
		data       []byte
		wantStatus int
	}{
		{"valid-add", validBook, http.StatusOK},
		{"valid-remove", removeBook, http.StatusOK},
		{"invalid-book", invalidBook, http.StatusBadRequest},
		{"invalid-action", invalidAction, http.StatusBadRequest},
		{"invalid-data", []byte(`\u1000`), http.StatusBadRequest},
		{"invalid-empty", nil, http.StatusBadRequest},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			srv := NewServer(settings, &mockBookLog{}, bk, rl, auth)
			w := sendRequestWithJWT("PUT", "/v1/booklog", bytes.NewReader(tt.data), srv)
			if w.Code != tt.wantStatus {
				t.Errorf("BookLogUpdate() expected HTTP status '%d', got: %v", tt.wantStatus, w.Code)
			}
		})
	}
}

func TestController_ChapterLog_BookLogs(t *testing.T) {
	// Set up the mock services
	settings := getSettings()
	bk := &mockBook{}
	rl := &mockReadingLog{}
	auth := &mockAuth{}

	validBook := []byte(`{"code":"ruth", "chapter":1}`)
	invalidBook := []byte(`{"code":"invalid", "chapter":1}`)

	tests := []struct {
		name       string
		data       []byte
		wantStatus int
	}{
		{"valid", validBook, http.StatusOK},
		{"invalid", invalidBook, http.StatusBadRequest},
		{"invalid-data", []byte(`\u1000`), http.StatusBadRequest},
		{"invalid-empty", nil, http.StatusBadRequest},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			srv := NewServer(settings, &mockBookLog{}, bk, rl, auth)
			w := sendRequestWithJWT("POST", "/v1/chapterlog", bytes.NewReader(tt.data), srv)
			if w.Code != tt.wantStatus {
				t.Errorf("ChapterLog() expected HTTP status '%d', got: %v", tt.wantStatus, w.Code)
			}

			w = sendRequestWithJWT("POST", "/v1/booklogs", bytes.NewReader(tt.data), srv)
			if w.Code != tt.wantStatus {
				t.Errorf("BookLogs() expected HTTP status '%d', got: %v", tt.wantStatus, w.Code)
			}
		})
	}
}

func TestController_BookLogRemove(t *testing.T) {
	settings := getSettings()
	bk := &mockBook{}
	rl := &mockReadingLog{}
	auth := &mockAuth{}

	tests := []struct {
		name       string
		id         int
		wantStatus int
	}{
		{"valid", 1, http.StatusOK},
		{"invalid", 99, http.StatusBadRequest},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			srv := NewServer(settings, &mockBookLog{}, bk, rl, auth)
			u := fmt.Sprintf("/v1/booklog/%d", tt.id)

			w := sendRequestWithJWT("DELETE", u, nil, srv)
			if w.Code != tt.wantStatus {
				t.Errorf("BookLog() expected HTTP status '%d', got: %v", tt.wantStatus, w.Code)
			}
		})
	}
}
