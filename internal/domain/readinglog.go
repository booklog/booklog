package domain

import "time"

// ReadingLog is the record of when a chapter was read
type ReadingLog struct {
	ID      int64     `json:"id"`
	User    string    `json:"user"`
	Book    string    `json:"code"`
	Chapter int       `json:"chapter"`
	Date    time.Time `json:"date"`
}
