package domain

import (
	"time"
)

// UserInfo holds the user details.
type UserInfo struct {
	Username  string    `json:"username"`
	Email     string    `json:"email"`
	Firstname string    `json:"firstname"`
	Lastname  string    `json:"lastname"`
	Role      int       `json:"role"`
	LastLogin time.Time `json:"lastLogin"`
	CreatedAt time.Time `json:"createdAt"`
	UpdatedAt time.Time `json:"updatedAt"`
}

// Roles that show the user's authorisation level.
const (
	RoleUnauthorised = 0
	RoleStandard     = 100
	RoleAdmin        = 911
)
