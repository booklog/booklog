package domain

const (
	// TotalBooks defines the total number of books in the Bible.
	TotalBooks = 1189
)

// Dashboard holds the data need to display the dashboard widgets.
type Dashboard struct {
	LastRead     ReadingLog `json:"lastRead"`
	ChaptersRead int64      `json:"chaptersRead"`
	Summary      []Book     `json:"summary"`
}
