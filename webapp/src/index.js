import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
//import reportWebVitals from './reportWebVitals';

const appTheme = createTheme({
    palette: {
        type: 'light',
        primary: {
            main: '#333333',
        },
        secondary: {
            main: 'rgba(98,98,98,0.75)',
        },
        text: {
            primary: '#333333',
        },
    },
});

ReactDOM.render(
    <React.Fragment>
        <ThemeProvider theme={appTheme}>
            <CssBaseline/>
            <App />
        </ThemeProvider>
    </React.Fragment>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
//reportWebVitals();
