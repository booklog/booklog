import React, { Component } from 'react';
import Header from './components/Header'
import Summary from './components/Summary'
import Login from './components/Login'
import API from './components/API'
import Container from '@mui/material/Container';
import {getSessionBook, getToken, isLoggedIn, removeToken, saveSessionBook} from './components/Utils'
import MainHero from "./components/MainHero";
import Register from "./components/Register";
import Index from "./components/Index";
import RecordLog from "./components/RecordLog";
import Books from "./components/Books";
import moment from "moment";


class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      token: props.token || {},
      selectedBook: getSessionBook,
      selectedChapter: 1,
      books: [],
      bookNames: Books,
      read: [],
      logs: [],
      summary: [],
      dashboard: {
        lastRead: {
          code: '', chapter:0, date:null,
        },
        summary: [],
        chaptersRead: 0,
      },
      year: moment().year(),
    }

    let p = this.getPath()
    this.dataForRoute(p)
  }

  getPath() {
    let pp = window.location.pathname.split('/')
    if (pp.length > 0) {
      return pp[1]
    } else {
      return null
    }
  }

  getBook = (b) => {
    API.bookLog(b, this.state.year).then(response => {
      this.setState({selectedBook: b, read: response.data.record.read})
      saveSessionBook(b)
    })
  }

  getBookLogs = (b) => {
    API.bookLogs(b, this.state.year).then(response => {
      this.setState({selectedBook: b, logs: response.data.records})
      saveSessionBook(b)
    }).catch(()=> {
      removeToken()
      this.setState({token: {}})
    })
  }

  getBookSummary = () => {
    API.bookSummary(this.state.year).then(response => {
      this.setState({summary: response.data.records})
    }).catch(()=> {
      removeToken()
      this.setState({token: {}})
    })
  }

  getDashboard = () => {
    API.dashboard(this.state.year).then(response => {
      this.setState({dashboard: response.data.record})
    }).catch(()=> {
      removeToken()
      this.setState({token: {}})
    })
  }

  handleChange = (book) => {
    saveSessionBook(book)
    this.setState({selectedBook: book, selectedChapter: 1}, ()=> {
      let p = this.getPath()
      this.dataForRoute(p)
    })
  }

  handleChapterClick = (chapter) => {
    API.bookLogUpdate('add', this.state.selectedBook,  chapter).then(response => {
      this.setState({selectedChapter: chapter}, () => {
        let p = this.getPath()
        this.dataForRoute(p)
      })
    })
  }

  handleYearChange = (year) => {
    this.setState({year: year}, () => {
      let p = this.getPath()
      this.dataForRoute(p)
    })
  }

  handleAddLog = () => {
    API.bookLogUpdate('add', this.state.selectedBook,  this.state.selectedChapter).then(response => {
      this.getBook(this.state.selectedBook)
      this.getBookLogs(this.state.selectedBook)
    })
  }

  handleRemoveLog = (id) => {
    API.bookLogRemove(id).then(() => {
      this.getBook(this.state.selectedBook)
      this.getBookLogs(this.state.selectedBook)
    })
  }

  dataForRoute(p) {
    let selected = getSessionBook()
    if(p==='update') {this.getBook(selected)}
    if(p==='update') {this.getBookLogs(selected)}
    //if(p==='summary') {this.getBookSummary()}
    if(p==='summary') {this.getDashboard()}
  }

  renderChart(p) {
    if (!p) return
    return <RecordLog books={this.state.bookNames} selectedBook={this.state.selectedBook} selectedChapter={this.state.selectedChapter} logs={this.state.logs} read={this.state.read}
                      onChange={this.handleChange} onClick={this.handleChapterClick} onAdd={this.handleAddLog} onRemove={this.handleRemoveLog}
                      year={this.state.year} onYear={this.handleYearChange} />
  }

  renderSummary(p) {
    if (!p) return
    return <Summary year={this.state.year} onYear={this.handleYearChange} dashboard={this.state.dashboard} onClick={this.handleClick} />
  }

  authCheck(p, token) {
    if ((!p) || (p==='login') || (p==='register')) {
      // no auth needed
      return
    }

    if (!isLoggedIn(token)) {
      window.location.pathname = "/login"
    }
  }

  render() {
    let p = this.getPath()
    this.authCheck(p, getToken())

    return (
        <React.Fragment>
            <Header token={getToken()} />
            {!p ? <MainHero />: ''}
            <Container maxWidth="lg">
              {!p ? <Index />: ''}

              {p==='login' ? <Login />: ''}
              {p==='register' ? <Register />: ''}

              {/* paths that need authorisation */}
              {p==='update' ? this.renderChart(p): ''}
              {p==='summary' ? this.renderSummary(p): ''}
            </Container>
          </React.Fragment>
    );
  }
}

export default App;
