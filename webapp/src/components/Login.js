import * as React from 'react';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import FormControl from '@mui/material/FormControl';
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import API from "./API";
import {Alert, Link} from "@mui/material";
import {saveToken, T} from "./Utils";
import Constants from "./Constants";


export default function Login() {
    const [email, setEmail] = React.useState('');
    const handleChangeEmail = (event) => {
        setEmail(event.target.value);
    };

    const [password, setPassword] = React.useState('');
    const handleChangePassword = (event) => {
        setPassword(event.target.value);
    };

    const [error, setError] = React.useState('');
    const handleLogin = (e) => {
        e.preventDefault();
        if (email.trim()==='') {
            setError('Email must be entered')
            return
        }
        if (password.trim()==='') {
            setError('Password must be entered')
            return
        }

        API.login(email.trim(), password.trim()).then((resp) => {
            let parts = resp.headers.authorization.split(' ')
            if (parts.length===2) {
                saveToken(parts[1])
                window.location.pathname = Constants.afterLogin
            }
        }).catch(e => {
            setError(e.response.data.code + ": " + e.response.data.message)
        })
    };

    const handleKeypress = e => {
        if (e.key === 'Enter') {
            handleLogin(e)
        }
    };

    return (
        <Box sx={{ my: 4 }}>
            <Typography variant="h4" component="h1">
                Sign-in
            </Typography>
            {error ? <Alert severity="error" sx={{ mb: 1}}>{error}</Alert>: ''}
            <FormControl sx={{ flexDirection: "column", width: 400 }}>
                <TextField id="outlined-username" required="required" label="Email" value={email} onChange={handleChangeEmail} onKeyPress={handleKeypress} sx={{ mb: 1}}/>
                <TextField id="outlined-password" required="required" autoComplete="current-password" type="password" label="Password" value={password} onChange={handleChangePassword} onKeyPress={handleKeypress} sx={{ mb: 1 }}/>
                <Button variant="contained" onClick={handleLogin}>Login</Button>
            </FormControl>
            <Typography>
                <br />
                Need to create an account? <Link href="/register">{T('sign-up')}</Link>
                <br />
                Forgot password? <Link href="/reset">{T('reset')}</Link>
            </Typography>
        </Box>
    );
}
