import * as React from 'react';
import {T} from "./Utils";
import Typography from "@mui/material/Typography";
import {
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow
} from "@mui/material";
import moment from "moment";
import Box from "@mui/material/Box";
import Constants from './Constants';
import Button from "@mui/material/Button";

export default function ChapterLogs(props) {

    const deleteClick = (e) => {
        e.preventDefault()
        let id = parseInt(e.target.getAttribute('data-key'))
        props.onRemove(id)
    }


    if (props.logs.length===0) {
        return (
            <Box>
                <Typography variant="body1" gutterTop>{T('no-records')}</Typography>
            </Box>
        )
    }
    return (
        <TableContainer>
            <Table aria-label={T('reading-history')}>
                <TableHead>
                    <TableRow>
                        <TableCell />
                        <TableCell>Date</TableCell>
                        <TableCell>Chapter</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {props.logs.map(l => (
                        <TableRow>
                            <TableCell>
                                <Button variant="outlined" onClick={deleteClick} data-key={l.id} sx={{color:Constants.selectedColor}}>
                                    X
                                </Button>
                            </TableCell>
                            <TableCell>{moment.utc(l.date).local().format('LLL')}</TableCell>
                            <TableCell>{l.chapter}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    )
}
