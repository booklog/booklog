import * as React from 'react';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import FormControl from '@mui/material/FormControl';
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import API from "./API";
import {Alert, Link} from "@mui/material";
import {T} from "./Utils";


export default function Register() {
    const [firstname, setFirstname] = React.useState('');
    const handleChangeFirstname = (event) => {
        setFirstname(event.target.value.trim());
    };

    const [lastname, setLastname] = React.useState('');
    const handleChangeLastname = (event) => {
        setLastname(event.target.value.trim());
    };


    const [email, setEmail] = React.useState('');
    const handleChangeEmail = (event) => {
        setEmail(event.target.value.trim());
    };

    const [password, setPassword] = React.useState('');
    const handleChangePassword = (event) => {
        setPassword(event.target.value.trim());
    };

    const [passwordConfirm, setPasswordConfirm] = React.useState('');
    const handleChangePasswordConfirm = (event) => {
        setPasswordConfirm(event.target.value.trim());
    };

    const [error, setError] = React.useState('');
    const handleRegister = (e) => {
        e.preventDefault();
        if (firstname.trim()==='') {
            setError('Firstname must be entered')
            return
        }
        if (lastname.trim()==='') {
            setError('Lastname must be entered')
            return
        }
        if (email.trim()==='') {
            setError('Email must be entered')
            return
        }
        if (password.trim()==='') {
            setError('Password must be entered')
            return
        }
        if (passwordConfirm.trim()==='') {
            setError('Password confirmation must be entered')
            return
        }
        if (password.trim()!==passwordConfirm.trim()) {
            setError('Passwords do not match')
            return
        }


        API.register(email.trim(), password.trim(), firstname.trim(), lastname.trim()).then(() => {
            window.location.pathname = "/login"
        }).catch(e => {
            setError(e.response.data.code + ": " + e.response.data.message)
        })
    };

    const handleKeypress = e => {
        if (e.key === 'Enter') {
            handleRegister(e)
        }
    };

    return (
        <Box sx={{ my: 4 }}>
            <Typography variant="h4" component="h1">
                Create a new account (it's free!)
            </Typography>
            {error ? <Alert severity="error" sx={{ mb: 1}}>{error}</Alert>: ''}
            <FormControl sx={{ flexDirection: "column", width: 400 }}>
                <TextField id="outlined-firstname" required="required" label="First name" value={firstname} onChange={handleChangeFirstname} onKeyPress={handleKeypress} sx={{ mb: 1}}/>
                <TextField id="outlined-lastname" required="required" label="Last name" value={lastname} onChange={handleChangeLastname} onKeyPress={handleKeypress} sx={{ mb: 1}}/>
                <TextField id="outlined-username" required="required" label="Email" value={email} onChange={handleChangeEmail} onKeyPress={handleKeypress} sx={{ mb: 1}}/>
                <TextField id="outlined-password" type="password"  required="required" label="Password" value={password} onChange={handleChangePassword} onKeyPress={handleKeypress} sx={{ mb: 1 }}/>
                <TextField id="confirm-password" type="password"  required="required" label="Confirm password" value={passwordConfirm} onChange={handleChangePasswordConfirm} onKeyPress={handleKeypress} sx={{ mb: 1 }}/>
                <Button variant="contained" onClick={handleRegister}>{T('sign-up')}</Button>
            </FormControl>
            <Typography>
                <br />
                Already have an account? <Link href="/login">{T('login')}</Link>
            </Typography>
        </Box>

    );
}
