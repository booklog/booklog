import {Tab, Tabs} from "@mui/material";
import React from "react";

export default function TabView(props) {
    const handleClick = (e) => {
        e.preventDefault()
        const key = e.target.getAttribute('data-key')
        props.onClick(key)
    }

    return (
        <Tabs value={props.selected} onClick={handleClick} sx={{mb:1}}>
            {
                props.tabs.map(t => {
                    return <Tab label={t.label} value={t.key} data-key={t.key}/>
                })
            }
        </Tabs>
    )
}