import Button from "@mui/material/Button";
import * as React from "react";


const youversion = {
    "gen": "GEN",
    "ex": "EXO",
    "lev": "LEV",
    "num": "NUM",
    "deut": "DEU",
    "jos": "JOS",
    "ruth": "RUT",
    "jud": "JDG",
    "1sam": "1SA",
    "2sam": "2SA",
    "1kin": "1KI",
    "2kin": "2KI",
    "1chr": "1CH",
    "2chr": "2CH",
    "ezra": "EZR",
    "neh": "NEH",
    "est": "EST",
    "job": "JOB",
    "psa": "PSA",
    "pro": "PRO",
    "ecc": "ECC",
    "ss": "SNG",
    "isa": "ISA",
    "jer": "JER",
    "lam": "LAM",
    "eze": "EZK",
    "dan": "DAN",
    "hos": "HOS",
    "joel": "JOL",
    "amos": "AMO",
    "oba": "OBA",
    "jon": "JON",
    "mic": "MIC",
    "nah": "NAM",
    "hab": "HAB",
    "zep": "ZEP",
    "hag": "HAG",
    "zec": "ZEC",
    "mal": "MAL",
    "mat": "MAT",
    "mark": "MRK",
    "luke": "LUK",
    "john": "JHN",
    "acts": "ACT",
    "rom": "ROM",
    "1cor": "1CO",
    "2cor": "2CO",
    "gal": "GAL",
    "eph": "EPH",
    "phi": "PHP",
    "col": "COL",
    "1th": "1TH",
    "2th": "2TH",
    "1tim": "1TI",
    "2tim": "2TI",
    "tit": "TIT",
    "philem": "PHM",
    "heb": "HEB",
    "jam": "JAS",
    "1pet": "1PE",
    "2pet": "2PE",
    "1joh": "1JN",
    "2joh": "2JN",
    "3joh": "2JN",
    "jude": "JUD",
    "rev": "REV",
}

export default function BibleLink(props) {
    const link = () => {
        return "https://bible.com/bible/111/" + youversion[props.book] + '.' + props.chapter
    }

    const color = props.read ? "#E95420" : "#333333"

    return (
        <Button variant="outlined" title={`Read ${props.chapter}`} sx={{margin:[1,1], color:color}}
                target="_blank" rel="noreferrer"
                href={link()}>{props.chapter}</Button>
    )
    
}