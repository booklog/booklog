import React from 'react'
import {getToken, isLoggedIn, removeToken, T} from './Utils'
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import Container from "@mui/material/Container";
import {Menu, MenuItem} from "@mui/material";

const pages = [];
const pagesAuth = ['summary', 'update'];
const settings = ['login'];
const settingsAuth = ['logout'];


const ResponsiveAppBar = () => {
  const [anchorElNav, setAnchorElNav] = React.useState(null);

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleNavPage = (e) => {
    handleCloseNavMenu()
    let key = e.target.getAttribute('data-key')
    if (key==='logout') {
      removeToken()
    }

    window.location.pathname = '/' + key
  }

  const buildIconButton = () => {
    let token = getToken()
    if (!isLoggedIn(token)) {
      return
    }
    return (
      <IconButton
          size="large"
          aria-label="account of current user"
          aria-controls="menu-appbar"
          aria-haspopup="true"
          onClick={handleOpenNavMenu}
          color="inherit"
      >
        <MenuIcon />
      </IconButton>
    )
  }

  const buildMenu = () => {
    let pp = pagesAuth
    let token = getToken()
    if (!isLoggedIn(token)) {
      return
    }

    return (
        <Menu
            id="menu-appbar" anchorEl={anchorElNav}
            anchorOrigin={{vertical: 'bottom', horizontal: 'left'}}
            keepMounted
            transformOrigin={{vertical: 'top', horizontal: 'left'}}
            open={Boolean(anchorElNav)}
            onClose={handleCloseNavMenu}
            sx={{display: { xs: 'block', md: 'none' }}}
        >
          {pp.map((page) => (
            <MenuItem key={page} data-key={page} onClick={handleNavPage}>
              <Typography data-key={page} textAlign="center">{T(page)}</Typography>
            </MenuItem>
           ))}
        </Menu>
    )
  };

  const buildButtons = () => {
    let pp = pages
    let token = getToken()
    if (isLoggedIn(token)) {
      pp = pagesAuth
    }

    return pp.map((page) => (
        <Button
            key={page}
            data-key={page}
            onClick={handleNavPage}
            sx={{ my: 2, color: 'white', display: 'block' }}
        >
          {T(page)}
        </Button>
    ))
  };

  const buildSettings = () => {
    let ss = settings
    let token = getToken()
    if (isLoggedIn(token)) {
      ss = settingsAuth
    }

    return ss.map((setting) => (
        <Button
            key={setting}
            data-key={setting}
            onClick={handleNavPage}
            sx={{ my: 2, color: 'white', display: 'block' }}
        >
          {T(setting)}
        </Button>
    ))
  }

  return (
      <AppBar position="static">
        <Container maxWidth="xl">
          <Toolbar disableGutters>
            <Typography
                variant="h6"
                noWrap
                component="div"
                sx={{ mr: 2, display: { xs: 'none', md: 'flex' } }}
            >
              {T('title')}
            </Typography>

            <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
                {buildIconButton()}
                {buildMenu()}
            </Box>
            <Typography variant="h6" noWrap component="div" sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
              {T('title')}
            </Typography>
            <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
              {buildButtons()}
            </Box>

            <Box sx={{ flexGrow: 0 }}>
              {buildSettings()}
            </Box>
          </Toolbar>
        </Container>
      </AppBar>
  );
};
export default ResponsiveAppBar;
