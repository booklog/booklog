import {Tab, Tabs} from "@mui/material";
import React from "react";
import moment from "moment";

let thisYear = moment().year()

export default function YearTabs(props) {
    const handleClick = (e) => {
        e.preventDefault()
        let year = parseInt(e.target.getAttribute('data-key'))
        props.onClick(year)
    }

    return (
        <Tabs value={thisYear-props.year} onClick={handleClick} sx={{mb:1}}>
            <Tab label={thisYear} data-key={thisYear}/>
            <Tab label={thisYear-1} data-key={thisYear-1}/>
            <Tab label={thisYear-2} data-key={thisYear-2}/>
        </Tabs>
    )
}