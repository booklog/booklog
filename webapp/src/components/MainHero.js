import * as React from 'react';
import Typography from "@mui/material/Typography";
import {Paper} from "@mui/material";
import Image from '../assets/images/mainpage.jpg'
import Container from "@mui/material/Container";
import {T} from "./Utils";

const paperContainer = {
        background: `#444444 url(${Image}) no-repeat center`,
        height: 200,
}


export default function MainHero() {
    return (
        <Paper style={paperContainer}>
            <Container maxWidth="lg" >
                <Typography variant="h4" component="h1" color="#eeeeee" sx={{textAlign: 'right', paddingTop: 20}}>
                    {T('main-hero')}
                </Typography>
            </Container>
        </Paper>
    )
}
