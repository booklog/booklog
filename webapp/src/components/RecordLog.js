import * as React from 'react';
import Button from "@mui/material/Button";
import {T} from "./Utils";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import ChapterLogs from "./ChapterLogs";
import {Divider, Drawer, ListItem} from "@mui/material";
import Toolbar from "@mui/material/Toolbar";
import TabView from "./TabView";
import BibleLink from "./BibleLink";

const selectedColor = '#0D4875'
const drawerWidth = 240;

const tabs = [{label:T('update'), key: 'update'}, {label:T('read'), key:'read'}]

export default function RecordLog(props) {
    const { window } = props;
    const [mobileOpen, setMobileOpen] = React.useState(false);
    const [selectedTab, setSelectedTab] = React.useState('update');

    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };

    const renderBooks = () => {
        return props.books.map(book => {
            if (book.code===props.selectedBook) {
                return <ListItem button sx={{margin:[1,1], backgroundColor:selectedColor, color: '#eeeeee'}} onClick={changeBook} data-key={book.code}>{T(book.code)}</ListItem>
            }
            return <ListItem button variant="outlined" sx={{margin:[1,1]}} onClick={changeBook} data-key={book.code}>{T(book.code)}</ListItem>
        })
    }

    const drawer = () => (
        <div>
            <Toolbar />
            <Divider />
            {renderBooks()}
        </div>
    )

    const renderChapters = () => {
        let chapter = 0
        return props.read.map(reads => {
            chapter++
            if (reads>0) {
                return <Button variant="contained" title={`Read ${reads} times`} sx={{backgroundColor:selectedColor, margin:[1,1]}} onClick={changeChapter} data-key={chapter}>{chapter}</Button>
            }
            return <Button variant="outlined" title={`Read ${reads} times`} sx={{margin:[1,1]}} onClick={changeChapter} data-key={chapter}>{chapter}</Button>
        })
    }

    const readChapters = () => {
        let chapter = 0
        return props.read.map((reads) => {
            chapter++
            return <BibleLink book={props.selectedBook} chapter={chapter} read={reads>0} />
        })
    }

    const changeBook = (e) => {
        e.preventDefault()
        props.onChange(e.target.getAttribute('data-key'))
    }

    const changeChapter = (e) => {
        e.preventDefault()
        props.onClick(parseInt(e.target.getAttribute('data-key')))
    }

    const container = window !== undefined ? () => window().document.body : undefined;

    const changeTab = (tab) => {
        setSelectedTab(tab)
    }

    const tabUpdate = () => {
        return (
            <Box>
                <Typography variant="h5" component="h2" gutterBottom sx={{ml: 2}}>{T('chapters')}</Typography>
                {renderChapters()}

                <Divider sx={{margin: [1,1]}} />

                <Typography variant="h5" component="h2" gutterTop gutterBottom sx={{ml: 2}}>{T('reading-history')}</Typography>
                <ChapterLogs logs={props.logs} onRemove={props.onRemove} />
            </Box>
        )
    }

    const tabRead = () => {
        return (
            <Box>
                <Typography variant="h5" component="h2" gutterBottom sx={{ml: 2}}>{T('read-chapter')}</Typography>
                {readChapters()}
            </Box>
        )
    }

    return (
        <Box sx={{ display: 'flex' }}>
            <Box
                component="nav"
                aria-label="books"
            >
                <Drawer
                    container={container}
                    variant="temporary"
                    open={mobileOpen}
                    onClose={handleDrawerToggle}
                    ModalProps={{
                        keepMounted: true,
                    }}
                    sx={{
                        display: { xs: 'block', sm: 'block' },
                        '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
                    }}
                >
                    {drawer()}
                </Drawer>
            </Box>
            <Box sx={{display: 'grid', mt:1, gridAutoColumns: '1fr',}}>
                <Box sx={{backgroundColor: '#eeeeee'}}>
                    <Toolbar>
                        <Typography variant="h4" noWrap component="h1" sx={{flexGrow:1}}>
                            {T(props.selectedBook)}
                        </Typography>
                        <Button
                            variant="contained"
                            aria-label="select book"
                            onClick={handleDrawerToggle}
                            sx={{ ml: 2, flexGrow:0, backgroundColor:selectedColor}}
                        >
                            {T('select-book')}
                        </Button>
                    </Toolbar>
                </Box>
                <Box >
                    <TabView selected={selectedTab} onClick={changeTab} tabs={tabs} />

                    {selectedTab==='update' && tabUpdate()}
                    {selectedTab==='read' && tabRead()}

                </Box>
            </Box>
        </Box>
    )
}