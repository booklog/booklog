import Messages from './Messages'
import API from "./API";
import jwtDecode from 'jwt-decode'

export function T(message) {
    const msg = Messages[message] || message;
    return msg
}

export function saveSessionBook(l) {
    localStorage.setItem('book', l);
}

export function getSessionBook() {
    return localStorage.getItem('book') || 'gen'
}

export function saveToken(t) {
    localStorage.setItem('token', t);
}

export function getToken() {
    return localStorage.getItem('token')
}

export function removeToken() {
    localStorage.removeItem('token')
}

export function getAuthToken(callback) {
    // Get a fresh token and return it to the callback
    // The token will be passed to the views
    API.token().then((resp) => {
        let jwt = resp.headers.authorization

        if (!jwt) {
            callback({})
            return
        }
        let token = jwtDecode(jwt)

        if (!token) {
            callback({})
            return
        }
        callback(token)
    }).catch(() => {
        // not logged in
        callback({})
    })
}

export function isLoggedIn(jwt) {
    if (!jwt) return false
    let token = jwtDecode(jwt)
    if (!token) return false
    if (!token.role) return false
    return (token.role >= 100)
}