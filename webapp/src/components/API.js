import axios from 'axios'
import Constants from './Constants'

const csrfHeader = 'x-csrf-token'

function fetchToken ()  {
    return axios.get(Constants.baseUrl +'token')
        .catch(e => {
            console.log(e)
        })
}

let wrapper = {
    get: (path) => {
        return axios.get(Constants.baseUrl + path)
    },

    getTokenGet: () => {
        return this.get('token')
    },

    post: (path, data) => {
        return fetchToken()
            .then((response) => {
                let config = {
                    headers: {
                        'x-csrf-token': response.headers[csrfHeader],
                    }
                }
                return axios.post(Constants.baseUrl + path, data, config)
        })
    },

    put: (path, data) => {
        return fetchToken()
            .then((response) => {
            let config = {
                headers: {
                    'x-csrf-token': response.headers[csrfHeader],
                }
            }
            return axios.put(Constants.baseUrl + path, data, config)
        })
    },

    delete: (path) => {
        return fetchToken()
            .then((response) => {
                let config = {
                    headers: {
                        'x-csrf-token': response.headers[csrfHeader],
                    }
                }
                return axios.delete(Constants.baseUrl + path, config)
            })
    },
}

let service = {
    token: () => {
        return wrapper.getTokenGet();
    },

    login: (email, password) => {
        return axios.post(Constants.baseUrl + 'login', {email:email, password: password});
    },

    register: (email, password, firstname, lastname) => {
        return axios.post(Constants.baseUrl + 'register', {email:email, password: password, firstname: firstname, lastname:lastname});
    },

    bookList: () => {
        return wrapper.post( 'books');
    },

    bookSummary: (year) => {
        let data = {}
        if (year) {
            data ={year: year}
        }
        return wrapper.post('books/summary', data);
    },

    dashboard: (year) => {
        let data = {}
        if (year) {
            data ={year: year}
        }
        return wrapper.post('dashboard', data);
    },

    bookLog: (b, year) => {
        return wrapper.post('booklog', {code:b, year: year});
    },

    bookLogs: (b, year) => {
        return wrapper.post('booklogs', {code:b, year: year});
    },

    bookLogUpdate: (action, b, ch, year) => {
        let data = {code: b, chapter: parseInt(ch, 10), action: action, year: year}
        return wrapper.put('booklog', data);
    },

    bookLogRemove: (id) => {
        return wrapper.delete('booklog/' + id)
    }
}

export default service