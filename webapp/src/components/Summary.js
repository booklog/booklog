import React from 'react';
import {T} from "./Utils";
import chroma from "chroma-js";
import {List, ListItem, ListItemText} from "@mui/material";
import Box from "@mui/material/Box";
import YearTabs from "./YearTabs";
import Dashboard from "./Dashboard";

const colors = chroma.scale(['#B5D8EF','#0D4875']).mode('lch').colors(20);
const boxSize = 15;

export default function Summary(props) {
    return (
        <div>
            <YearTabs year={props.year} onClick={props.onYear} />
            <h1>{T('summary-header')}</h1>

            <Dashboard dashboard={props.dashboard} />

            <List sx={{ width:'100%'}}>
                {
                    createRows(props.dashboard.summary).map(row => {
                        return (
                            <ListItem>
                                <ListItemText sx={{width:100}} primary={row.book} />
                                <ListItem>{row.chapters}</ListItem>
                            </ListItem>
                        )
                    })
                }
            </List>
        </div>
    );
}

function createRows(logs) {
    return logs.map(b => {
        let i = 0;
        let book = T(b.code)
        let chapters = (
            <Box sx={{display:'flex', flexWrap:'wrap',p:0,m:0}}>
                {b.read.map(r => {
                    let j = r;
                    if (j===0) {
                        return <Box sx={{height:boxSize, width:boxSize, border:'1px solid #cccccc'}} />
                    }

                    // set heatmap color
                    if (j > 19) {j = 19}
                    let c = colors[j];
                    i += 1;

                    return <Box sx={{height:boxSize, width:boxSize, bgcolor:c, border:'1px solid #cccccc'}} />
                })}
            </Box>)

        return {id:i, book:book, chapters: chapters}
    })
}
