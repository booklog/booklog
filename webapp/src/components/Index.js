import * as React from 'react';
import Box from '@mui/material/Box';
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import {Card, CardContent} from "@mui/material";


export default function Index() {
    return (
        <Box sx={{ my: 4 }}>
            <Typography variant="h4" component="h1" gutterBottom>
                Track your Bible reading journey over time
            </Typography>
            <Card sx={{ minWidth: 275, backgroundColor: "#838282"}}>
                <CardContent>
                    <Typography variant="h7" component="div" color="#f0f0f0" gutterBottom>
                        Keep this Book of the Law always on your lips; meditate on it day and night, so that you may be careful
                        to do everything written in it. Then you will be prosperous and successful.
                    </Typography>
                    <Typography gutterBottom sx={{ fontSize: 14 }} color="#f0f0f0">
                        Joshua 1:8 NIV
                    </Typography>
                </CardContent>
            </Card>
            <Typography variant="body1" sx={{mt: 2, mb: 2}}>
                Life-changing encounters with God are available through reading His word. The Holy Spirit takes the
                Word of God and applies it to our lives, revealing Jesus to us, and encouraging. challenging, refining,
                and empowering us for life. Regular and consistent Bible reading is something that we cannot afford to
                neglect.
                <br /><br />
                Whether we follow a Bible plan or take a more fluid approach to our reading, it's worth tracking our
                progress through the scriptures so we take all the God is wanting to reveal to us. The bibleplan.co
                provides tools to visualize our reading, and to journal our thoughts during the process.
                <br /><br />
                It's free! There is no paid plan! Get started today.
            </Typography>
            <Typography variant="body1" gutterBottom>
                <Button variant="contained" href="/register" size="large">Sign-up</Button>
            </Typography>
        </Box>
    );
}
