import {T} from "./Utils";
import {Grid} from "@mui/material";
import React from "react";
import DashboardTile from "./DashboardTile";




export default function Dashboard(props) {
    return (
        <div>
            <Grid container sx={{ width:'100%'}} spacing={1}>
                <DashboardTile title={T('last-read')} text={T(props.dashboard.lastRead.code) + ' ' + props.dashboard.lastRead.chapter} />
                <DashboardTile title={T('chapters-read')} text={props.dashboard.chaptersRead} />
            </Grid>
        </div>
    );
}




// last read
// chapters read
// reading over time