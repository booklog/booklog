import React from 'react';
import {Grid, Paper} from "@mui/material";
import Typography from "@mui/material/Typography";

function DashboardTile(props) {
    return (
        <Grid item xs>
            <Paper elevation={4} sx={{ padding: 1}}>
                <Typography align="center" variant="button" display="block" gutterBottom>{props.title}</Typography>
                <Typography align="center" variant="h3" component="div" gutterBottom color="#E95420">{props.text}</Typography>
            </Paper>
        </Grid>
    );
}

export default DashboardTile;
